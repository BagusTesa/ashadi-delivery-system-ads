<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pengiriman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengiriman', function(Blueprint $table){
          $table->uuid('token');
          $table->string('nama_pengirim');
          $table->string('alamat_pengirim');
          $table->integer('kota_pengirim')->unsigned();
          $table->string('telepon_pengirim')->nullable();
          $table->string('nama_penerima');
          $table->string('alamat_penerima');
          $table->integer('kota_penerima')->unsigned();
          $table->string('telepon_penerima')->nullable();
          $table->string('email_penerima')->nullable();
          $table->double('lat_penerima', 16, 8)->nullable();
          $table->double('long_penerima', 16, 8)->nullable();
          $table->double('volume_barang', 5, 3);
          $table->double('berat_barang', 5, 3);
          $table->boolean('emailkonfirmasi')->default(false);
          $table->integer('kantor_id')->unsigned();
          $table->integer('statuspengiriman_id')->unsigned();
          $table->timestamps();

          $table->primary('token');
          $table->foreign('kantor_id')->references('id')->on('kantordistribusi');
          $table->foreign('kota_pengirim')->references('id')->on('kota');
          $table->foreign('kota_penerima')->references('id')->on('kota');
          $table->foreign('statuspengiriman_id')->references('id')->on('statuspengiriman');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pengiriman');
    }
}
