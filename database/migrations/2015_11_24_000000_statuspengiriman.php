<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Statuspengiriman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuspengiriman', function(Blueprint $table){
          $table->increments('id');
          $table->string('status');
          $table->string('deskripsi');
          $table->string('boxclass')->nullable();
          $table->string('labelclass')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('statuspengiriman');
    }
}
