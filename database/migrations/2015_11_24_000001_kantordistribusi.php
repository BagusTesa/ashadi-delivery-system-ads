<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kantordistribusi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kantordistribusi', function(Blueprint $table){
          $table->increments('id');
          $table->string('nama');
          $table->string('alamat');
          $table->integer('kota_id')->unsigned();
          $table->string('telepon');
          //tempat spesifik kantor cabang, by default mengikuti kota
          $table->double('lat', 16, 8);
          $table->double('long', 16, 8);
          $table->timestamps();

          $table->foreign('kota_id')->references('id')->on('kota');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kantordistribusi');
    }
}
