<?php

use Illuminate\Database\Seeder;

class PengirimanExSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Pengiriman::class, 300)->create();
    }
}
