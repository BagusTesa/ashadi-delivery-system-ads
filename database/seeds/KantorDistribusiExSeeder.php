<?php

use Illuminate\Database\Seeder;

class KantorDistribusiExSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('kantordistribusi')->insert([
        'nama'    => 'Universitas Widya Dharma',
        'alamat'  => 'Jalan Ki Hajar Dewantara, Klaten Utara, Karanganom, Klaten Utara, Klaten, Jawa Tengah 57400, Indonesia',
        'kota_id' => '106',
        'telepon' => '+62 272 322363',
        'lat'     => '-7.6928206',
        'long'    => '110.6223428',
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ]);
      DB::table('kantordistribusi')->insert([
        'nama'    => 'Institut Sepuluh November',
        'alamat'  => 'Campus ITS Sukolilo, Jalan Raya ITS, Keputih, Surabaya, Jawa Timur 60111, Indonesia',
        'kota_id' => '266',
        'telepon' => '+62 31 5994251',
        'lat'     => '-7.2769471',
        'long'    => '112.7894741',
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ]);
      DB::table('kantordistribusi')->insert([
        'nama'    => 'Institut Teknologi Bandung',
        'alamat'  => 'Jl. Ganesha No.10, Coblong, Kota Bandung, Jawa Barat 40132, Indonesia',
        'kota_id' => '14',
        'telepon' => '+62 274 6492599',
        'lat'     => '-6.89148',
        'long'    => '107.6084704',
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ]);
    }
}
