<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class StatusPengirimanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('statuspengiriman')->insert([
        'id'        => 1,
        'status'    => 'Tertunda',
        'deskripsi' => 'Paket masih berada di outlet kami',
        'boxclass'  => 'bg-yellow',
        'labelclass'=> 'label-warning'
      ]);
      DB::table('statuspengiriman')->insert([
        'id'        => 2,
        'status'    => 'Perjalanan',
        'deskripsi' => 'Paket sedang dikirimkan',
        'boxclass'  => 'bg-aqua',
        'labelclass'=> 'label-primary'
      ]);
      DB::table('statuspengiriman')->insert([
        'id'        => 3,
        'status'    => 'Terkirim',
        'deskripsi' => 'Paket telah dikirimkan',
        'boxclass'  => 'bg-green',
        'labelclass'=> 'label-success'
      ]);
      DB::table('statuspengiriman')->insert([
        'id'        => 4,
        'status'    => 'Gagal',
        'deskripsi' => 'Paket gagal dikirimkan',
        'boxclass'  => 'bg-red',
        'labelclass'=> 'label-danger'
      ]);
    }
}
