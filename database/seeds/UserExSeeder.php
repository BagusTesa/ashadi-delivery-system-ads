<?php

use Illuminate\Database\Seeder;

class UserExSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name'       => 'Admin Yogya',
        'email'      => 'adminyk@localhost.loli',
        'password'   => bcrypt('aman123'),
        'kantor_id'  => 1,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ]);
      DB::table('users')->insert([
        'name'       => 'Admin K',
        'email'      => 'adminkl@localhost.loli',
        'password'   => bcrypt('aman123'),
        'kantor_id'  => 2,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ]);
      DB::table('users')->insert([
        'name'       => 'Admin ITS',
        'email'      => 'adminits@localhost.loli',
        'password'   => bcrypt('aman123'),
        'kantor_id'  => 3,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ]);
      DB::table('users')->insert([
        'name'       => 'Admin ITB',
        'email'      => 'adminitb@localhost.loli',
        'password'   => bcrypt('aman123'),
        'kantor_id'  => 4,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
      ]);
    }
}
