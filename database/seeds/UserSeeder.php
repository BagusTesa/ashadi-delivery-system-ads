<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'name'       => 'Super User',
          'email'      => 'superuser@localhost.loli',
          'password'   => bcrypt('aman123'),
          'kantor_id'  => null,
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now()
        ]);

    }
}
