<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(StatusPengirimanSeeder::class);
        $this->call(KotaSeeder::class);
        $this->call(KantorDistribusiSeeder::class);
        $this->call(UserSeeder::class);

        //everything below this line is not an obligatory seeder
        $this->call(KantorDistribusiExSeeder::class);
        $this->call(UserExSeeder::class);
        $this->call(PengirimanExSeeder::class);

        //well, this line below is required by laravel, statement above is false
        Model::reguard();
    }
}
