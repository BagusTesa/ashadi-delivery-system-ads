<?php

use Illuminate\Database\Seeder;

class KantorDistribusiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kantordistribusi')->insert([
          'nama'    => 'Universitas Gadjah Mada',
          'alamat'  => 'Bulaksumur, Kec. Depok, Sleman, Daerah Istimewa Yogyakarta 55281, Indonesia',
          'kota_id' => '308',
          'telepon' => '+62 274 6492599',
          'lat'     => '-7.7713847',
          'long'    => '110.3753111',
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
