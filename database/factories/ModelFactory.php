<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Pengiriman::class, function(Faker\Generator $faker){
  $randKotaRef   = ['106', '266', '14', '308'];
  $kotaPenerima  = App\Models\Kota::find($randKotaRef[mt_rand(0,3)]);
  $randStatusRef = ['1', '2', '3', '4'];
  $dateUpdate    = $faker->dateTime;
  return [
    'nama_pengirim'       => $faker->name,
    'alamat_pengirim'     => $faker->address,
    'kota_pengirim'       => $randKotaRef[mt_rand(0,3)],
    'telepon_pengirim'    => $faker->phoneNumber,
    'nama_penerima'       => $faker->name,
    'alamat_penerima'     => $faker->address,
    'kota_penerima'       => $kotaPenerima->id,
    'telepon_penerima'    => $faker->phoneNumber,
    'email_penerima'      => $faker->email,
    'lat_penerima'        => $kotaPenerima->lat + ((mt_rand(-10,10)/mt_rand(2,7))/100),
    'long_penerima'       => $kotaPenerima->long + ((mt_rand(-10,10)/mt_rand(2,7))/100),
    'volume_barang'       => rand(1,7),
    'berat_barang'        => rand(2,5),
    'kantor_id'           => mt_rand(1,4),
    'statuspengiriman_id' => mt_rand(1,4),
    'created_at'          => $faker->dateTime($dateUpdate),
    'updated_at'          => $dateUpdate
  ];
});
