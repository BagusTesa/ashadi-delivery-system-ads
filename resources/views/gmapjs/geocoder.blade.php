<script>
//geocoder can't be used with probe-marker, need to build better code frameworks for this
var marker   = null;
var geocoder = null;
function placeProbe(_lat, _lng, _title){
  var coord = {lat: _lat, lng: _lng};
  var _marker = new google.maps.Marker({
    position: coord,
    map: map,
    title: _title,
    @if(isset($outLat) && isset($outLong))
      draggable: true
    @endif
  });
  return _marker;
}
$(document).ready(function(){
  geocoder = new google.maps.Geocoder();
});

function panMapTo(_lat, _long){
  map.panTo({lat: _lat,lng: _long});
}

function panMapToMarker(){
  map.panTo({lat: marker.getPosition().lat(), lng: marker.getPosition().lng()});
}

function updateField(fieldName, value){
  $(fieldName).val(value);
}

@if(isset($parameter) && isset($trigger))
  function geocode(){
    if(geocoder){
      var address = $("{{ $parameter }}").val();
      geocoder.geocode({'address': address}, function(results, status){
        if(status === google.maps.GeocoderStatus.OK){
          if(marker != null){
            marker.setMap(null);
          }
          marker = placeProbe(results[0].geometry.location.lat(), results[0].geometry.location.lng(), address);
          @if(isset($outLat) && isset($outLong))
            $("{{ $outLat }}").val(marker.getPosition().lat());
            $("{{ $outLong }}").val(marker.getPosition().lng());
            google.maps.event.addListener(marker, 'dragend', function(){
              updateField("{{ $outLat }}", marker.getPosition().lat());
              updateField("{{ $outLong }}", marker.getPosition().lng());
              panMapToMarker();
            });
          @endif
          google.maps.event.addListener(map, 'tilesloaded', function(){
            panMapToMarker();
          });
          panMapToMarker(marker);
        }
        else{
          alert("{{ trans('ads.map.failed') }}");
        }
      });
    }
  }
  $("{{ $trigger }}").click(function($this){
    geocode();
  });
  @if(isset($lat) && isset($lng))
    $(document).ready(function(){
      marker = placeProbe({{ $lat }}, {{ $lng }}, "Kota");
    });
  @endif
@endif
</script>
