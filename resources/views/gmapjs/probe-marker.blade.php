<script>
  var marker = null;
  function placeProbe(_lat, _lng, _title){
    var coord = {lat: _lat, lng: _lng};
    var _marker = new google.maps.Marker({
      position: coord,
      map: map,
      title: _title,
      @if(isset($outLat) && isset($outLong))
        draggable: true
      @endif
    });
    return _marker;
  }
  //not working yet, idk, lol
  function updateField(fieldName, value){
    $(fieldName).val(value);
  }

  function panMapTo(_lat, _long){
    map.panTo({lat: _lat,lng: _long});
  }

  function panMapToMarker(){
    map.panTo({lat: marker.getPosition().lat(), lng: marker.getPosition().lng()});
  }

  @if(isset($parameter))
    //initialize
    $(document).ready(function(){
      var ref = $("{{ $parameter }}").children("option").filter(":selected");
      var _lat = Number(ref.attr("lat"));
      var _long = Number(ref.attr("long"));
      marker = placeProbe(_lat, _long, ref.text());

      @if(isset($outLat) && isset($outLong))
        updateField("{{ $outLat }}", _lat);
        updateField("{{ $outLong }}", _long);
        google.maps.event.addListener(marker, 'dragend', function(){
          updateField("{{ $outLat }}", marker.getPosition().lat());
          updateField("{{ $outLong }}", marker.getPosition().lng());
          panMapToMarker();
        });
      @endif

      google.maps.event.addListener(map, 'tilesloaded', function(){
        panMapToMarker();
      });
      panMapToMarker();
    });
    //changes
    @if(isset($outLat) && isset($outLong))
      $("{{ $parameter }}").change(function(){
        var refop = $(this).children("option").filter(":selected");
        var _lat = Number(refop.attr("lat"));
        var _long = Number(refop.attr("long"));
        var pos = {
          lat: _lat,
          lng: _long
        }
        marker.setPosition(pos);
        updateField("{{ $outLat }}", _lat);
        updateField("{{ $outLong }}", _long);

        panMapToMarker();
      });
    @endif
  @endif
</script>
