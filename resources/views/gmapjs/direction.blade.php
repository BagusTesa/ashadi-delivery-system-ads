<script>
//directions can't be used with probe-marker and even geocoder, need to build better code frameworks for this
var route            = null;
var marker1          = null;
var marker2          = null;
var directionService = null;
var directionRenderer= null;
function placeProbe(_lat, _lng, _title){
  var coord = {lat: _lat, lng: _lng};
  var _marker = new google.maps.Marker({
    position: coord,
    map: map,
    title: _title,
    @if(isset($outLat) && isset($outLong))
      draggable: true
    @endif
  });
  return _marker;
}
$(document).ready(function(){
  directionService = new google.maps.DirectionsService();
  directionRenderer = new google.maps.DirectionsRenderer();
  directionRenderer.setMap(map);
  marker1 = placeProbe({{ $startLat }}, {{ $startLng }}, "{{ $startName }}");
  marker2 = placeProbe({{ $endLat }}, {{ $endLng }}, "{{ $endName }}");

  @if(isset($startLat) && isset($startLng) && isset($endLat) && isset($endLng) && isset($startName) && isset($endName))
  if(directionService){
    request = {
      origin: {lat: {{ $startLat }}, lng: {{ $startLng }} },
      destination: {lat: {{ $endLat }}, lng: {{ $endLng }} },
      travelMode: google.maps.TravelMode.DRIVING
    };
    directionService.route(request, function(results, status){
      if(status === google.maps.DirectionsStatus.OK){
        route = results;
        directionRenderer.setDirections(route);
        if(marker1 != null){
          marker1.setMap(null);
          if(marker2 != null){
            marker2.setMap(null);
          }
        }
        marker1 = placeProbe({{ $startLat }}, {{ $startLng }}, "{{ $startName }}");
        marker2 = placeProbe({{ $endLat }}, {{ $endLng }}, "{{ $endName }}");
        panMapToMarker(marker1);
      }
      else{
        alert("{{ trans('ads.map.failed') }}");
      }
    });
  }
  @endif
});

function panMapTo(_lat, _long){
  map.panTo({lat: _lat,lng: _long});
}

function panMapToMarker(){
  map.panTo({lat: marker1.getPosition().lat(), lng: marker1.getPosition().lng()});
}
</script>
