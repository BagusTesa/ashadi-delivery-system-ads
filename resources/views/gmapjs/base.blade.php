
    <script>
      var map;
      function initMap(){
        map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: {lat: @if(isset($lat)){{ $lat }}@else{{ '-34.397' }}@endif, lng: @if(isset($lng)){{ $lng }}@else{{ '150.644' }}@endif},
          zoom: 12
        });
      }
      // Auto re-render map on resize
      $('#map-canvas').bind('resize', function(){
        google.maps.event.trigger(map, 'resize')
      });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('ads.GMAP_APIKEY') }}&callback=initMap"></script>
