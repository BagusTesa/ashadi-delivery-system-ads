@extends('templates.email-base')
@section('meta')

@endsection
@section('title')
 Informasi Pengiriman Paket
@endsection
@section('recipient')
 {{ $pengiriman->nama_penerima }}
@endsection
@section('page')
  Paket ID: {{ $pengiriman->token }}<br>
  Dengan rincian sebagai berikut:
  <table>
    <tr>
      <td>Nama Pengirim</td>
      <td>{{ $pengiriman->nama_pengirim }}</td>
    </tr>
    <tr>
      <td>Alamat Pengirim</td>
      <td>{{ $pengiriman->alamat_pengirim }}</td>
    </tr>
    <tr>
      <td>Kota Pengirim</td>
      <td>{{ $pengiriman->asal->nama }}</td>
    </tr>
    <tr>
      <td>Telepon Pengirim</td>
      <td>{{ $pengiriman->telepon_pengirim }}</td>
    </tr>
    <tr>
      <td>Lokasi Terakhir</td>
      <td>{{ $pengiriman->kantor->kota->nama }}</td>
    </tr>
    <tr>
      <td>Status</td>
      <td>{{ $pengiriman->status->status }}</td>
    </tr>
  </table>
  Yang dikirimkan melalui kantor kami pada tanggal {{ $pengiriman->created_at }}. Untuk membantu kelancaran proses pengiriman,
  kami memohon bpk/ibu/sdr untuk melihat status pengiriman paket yang ditujukan pada bpk/ibu/sdr melalui
  <a href="{{ route('tracking') }}">halaman pelacakan kami</a>.
@endsection
