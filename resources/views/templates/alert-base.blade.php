@if(session()->has('danger') || session()->has('warning') || session()->has('success'))
  <div class="alert
  @if(session()->has('danger'))
  {{ 'alert-danger' }}
  @elseif(session()->has('warning'))
  {{ 'alert-warning' }}
  @elseif(session()->has('success'))
  {{ 'alert-success' }}
  @endif
   alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
    <b>@if(session()->has('danger'))
    {{ session()->pull('danger') }}
    @elseif(session()->has('warning'))
    {{ session()->pull('warning') }}
    @elseif(session()->has('success'))
    {{ session()->pull('success') }}
    @endif</b>
  </div>
@endif
