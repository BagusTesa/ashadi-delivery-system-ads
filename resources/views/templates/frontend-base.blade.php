@extends('templates.master-base')
@section('css')
  <link rel="stylesheet" href="assets/css/frontend.css">
  
@endsection
@section('js')

@endsection
@section('body-class') @show
@section('body')
  @include('templates.frontend-header')
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="min-height: 70vh;">
        <h3>@yield('title')</h3>
        <hr>
        <p>
          @yield('subtitle')
        </p>
        @section('page')

        @show
      </div>
    </div>
    @include('templates.frontend-footer')
  </div>
@endsection
