<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    @section('meta')

    @show
    <title>Ashadi Delivery System | @yield('title')</title>
  </head>
  <body>
    <div>
      Yth. bpk/ibu/sdr @yield('recipient')
    </div>
    <div>
      @section('page')

      @show
    </div>
    <div>
      Terima kasih atas perhatian bpk/ibu/sdr.
      Salam Hangat dari kami,
      Tim Ashadi Delivery System
    </div>
  </body>
</html>
