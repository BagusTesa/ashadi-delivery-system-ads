@extends('templates.master-base')
@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/AdminLTE/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/AdminLTE/dist/css/skins/skin-black.min.css') }}">
@endsection
@section('js')
  <script src="{{ asset('vendor/AdminLTE/dist/js/app.min.js') }}"></script>
@endsection
@section('body-class'){{ 'hold-transition skin-black sidebar-mini' }}@append
@section('body')
<div class="wrapper">
  @include('templates.backend-header')
  @include('templates.backend-sidebar')
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
      </h1>
    </section>
    <section class="content">
      <div class="row">
        @section('page')

        @show
      </div>
    </section>
  </div>
  @include('templates.backend-footer')
</div>
@endsection
