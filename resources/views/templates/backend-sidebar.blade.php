<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu">
      <li class="header">Menu</li>
      <li class="@if(Request::url() == route('admin.index')){{ 'active' }}@endif"><a href="{{ route('admin.index') }}"><i class="fa fa-envelope-o"></i> <span>Dashboard</span></a></li>
      {{-- Ordinary user --}}
      @if(Auth::user()->kantor()->first() != null)
      <li class="@if(Request::url() == route('admin.pengiriman.create')){{ 'active' }}@endif"><a href="{{ route('admin.pengiriman.create') }}"><i class="fa fa-pencil-square-o"></i> <span>Pengiriman Baru</span></a></li>
      <li class="@if((Request::url() == route('admin.pengiriman.index')) || Request::is('admin/pengiriman/*/edit')){{ 'active' }}@endif"><a href="{{ route('admin.pengiriman.index') }}"><i class="fa fa-cube"></i> <span>Pengiriman</span></a></li>
      <li class="treeview @if(Request::is('admin/paket*')){{ 'active' }}@endif">
        <a href="#"><i class="fa fa-send-o"></i><span>Mobilitas Paket</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="{{ route('admin.getpaketmasuk') }}">Paket Masuk</a></li>
          <li><a href="{{ route('admin.getpaketkeluar') }}">Paket Keluar</a></li>
        </ul>
      </li>
      @endif
      <li class="treeview @if(Request::is('admin/*daftar-pengiriman*')){{ 'active' }}@endif">
        <a href="#"><i class="fa fa-cubes"></i> <span>Status Pengiriman</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="{{ route('admin.all-pengiriman') }}">All</a></li>
          @foreach(\App\Models\StatusPengiriman::all() as $status_pengiriman)
          <li><a href="{{ route('admin.status-pengiriman', $status_pengiriman->id) }}">{{ $status_pengiriman->status }}</a></li>
          @endforeach
        </ul>
      </li>
      {{-- Super Admin only (those who had kantor_id = null) --}}
      @if(Auth::user()->kantor()->first() == null)
      <li class="treeview @if(Request::is('admin/user*')){{ 'active' }}@endif">
        <a href="#"><i class="fa fa-user"></i><span>Manajemen User</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="{{ route('admin.user.index') }}">Daftar User</a></li>
          <li><a href="{{ route('admin.user.create') }}">Tambah User</a></li>
        </ul>
      </li>
      <li class="treeview @if(Request::is('admin/kantordistribusi*')){{ 'active' }}@endif">
        <a href="#"><i class="fa fa-building"></i><span>Manajemen Kantor</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="{{ route('admin.kantordistribusi.index') }}">Daftar Kantor</a></li>
          <li><a href="{{ route('admin.kantordistribusi.create') }}">Tambah Kantor</a></li>
        </ul>
      </li>
      <li class="treeview @if(Request::is('admin/kota*')){{ 'active' }}@endif">
        <a href="#"><i class="fa fa-map-o"></i><span>Manajemen Kota</span><i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="{{ route('admin.kota.index') }}">Daftar Kota</a></li>
          <li><a href="{{ route('admin.kota.create') }}">Tambah Kota</a></li>
        </ul>
      </li>
      @endif
    </ul>
  </section>
</aside>
