@if(isset($parameter) && ($errors->has($parameter)))
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
    <b>{{ $errors->first($parameter) }}</b>
  </div>
@endif
