@extends('templates.master-base')
@section('css')
  <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/AdminLTE/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/AdminLTE/dist/css/skins/skin-black.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/popup.css') }}">
@endsection
@section('js')
  <script src="{{ asset('vendor/AdminLTE/dist/js/app.min.js') }}"></script>
@endsection
@section('body-class'){{ 'skin-black' }}@append
@section('body')
<div class="wrapper">
  <div class="content-wrapper pop-up">
    <section class="content-header">
      <h1>
      @yield('title')
      <small>@yield('subtitle')</small>
      </h1>
    </section>
    <section class="content">
      @section('page')

      @show
    </section>
  </div>
</div>
@endsection
