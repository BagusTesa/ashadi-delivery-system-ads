<header class="navbar navbar-static-top navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ route('home') }}"><b>Ashadi</b> Delivery System</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="@if(Request::url() == route('home')){{ 'active' }}@endif"><a href="{{ route('home') }}">Beranda</a></li>
        <li class="@if(Request::url() == route('kantor')){{ 'active' }}@endif"><a href="{{ route('kantor') }}">Kantor Cabang</a></li>
        <li class="@if(Request::url() == route('tracking')){{ 'active' }}@endif"><a href="{{ route('tracking') }}">Tracking</a></li>
        <li class="@if(Request::url() == route('aboutus')){{ 'active' }}@endif"><a href="{{ route('aboutus') }}">Tentang Kami</a></li>
      </ul>
    </div>
  </div>
</header>
