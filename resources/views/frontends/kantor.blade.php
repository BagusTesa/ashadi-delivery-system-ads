@extends('templates.frontend-base')
@section('css')
 @parent

@endsection
@section('js')
 @parent

@endsection
@section('title')
  Kantor Cabang
@endsection
@section('subtitle')
  Kami memiliki banyak cabang tersebar luas di indonesia. Berikut adalah daftarnya:
@endsection
@section('page')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Cabang Kami</h3>
  </div>
  <div class="box-body">
    <table class="table table-hover">
      <?php $point = 0 + ($kantor->currentPage() - 1) * $kantor->perPage(); ?>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>Kota</th>
        <th>Telepon</th>
      </tr>
      @foreach($kantor as $k)
        <tr>
          <td>{{ ++$point }}</td>
          <td>{{ $k->nama }}</td>
          <td>{{ $k->alamat }}</td>
          <td>{{ $k->kota->nama }}</td>
          <td>{{ $k->telepon }}</td>
        </tr>
      @endforeach
    </table>
  </div>
  <div class="text-center">
    {!! $kantor->render() !!}
  </div>
</div>
@endsection
