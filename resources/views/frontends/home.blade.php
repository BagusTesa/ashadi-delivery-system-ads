@extends('templates.frontend-base')
@section('css')
 @parent

@endsection
@section('js')
 @parent

@endsection
@section('title')
  Beranda
@endsection
@section('page')
@include('templates.alert-base')
<div class="row">
  @foreach($statuspengiriman as $status)
  <div class="col-lg-3 col-xs-6">
    <div class="jumbotron">
      <div class="row">
        <h3>{{ count($status->pengiriman) }}</h3><small class="label {{ $status->labelclass }}">{{ $status->status }}</small>
      </div>
    </div>
  </div>
  @endforeach
</div>
@endsection
