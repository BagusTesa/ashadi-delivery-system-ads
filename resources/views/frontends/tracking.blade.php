@extends('templates.frontend-base')
@section('css')
 @parent

@endsection
@section('js')
 @parent

@endsection
@section('title')
  Tracking
@endsection
@section('subtitle')

@endsection
@section('page')
<form action="{{ route('tracking') }}" method="post">
  {!! csrf_field() !!}
  <div class="row">
    <div class="col-sm-offset-3 col-sm-6">
      @include('templates.validation-base', ['parameter' => 'token'])
      <div class="input-group">
        <input type="text" name="token" class="form-control" placeholder="ID Paket" value"@include('templates.value-input-template', ['parameter' => 'token'])">
        <span class="input-group-btn">
          <button class="btn btn-default" type="submit">Temukan</button>
        </span>
      </div>
    </div>
  </div>
</form>
@if(isset($pengiriman))
<hr>
<div class="row">
  <div class="box-body table-responsive no-padding">
    <table class="table table-hover">
      <tr>
        <th>ID</th>
        <th>Penerima</th>
        <th>Tanggal</th>
        <th>Status</th>
        <th>Konfirmasi</th>
      </tr>
      <tr>
        <td>{{ $pengiriman->token }}</td>
        <td>{{ $pengiriman->nama_penerima }}</td>
        <td>{{ $pengiriman->created_at }}</td>
        <td><span class="label {{ $pengiriman->status->labelclass }}">{{ $pengiriman->status->status }}</span></td>
        <td>@if($pengiriman->emailkonfirmasi == true){{ 'Terkonfirmasi' }}@else{{ 'Belum dikonfirmasi' }}@endif</td>
      </tr>
    </table>
  </div>
</div>
@endif
@endsection
