@extends('templates.frontend-base')
@section('css')
 @parent
 <link rel="stylesheet" href="{{ asset('assets/css/map-canvas.css') }}">
@endsection
@section('js')
 @parent
 <?php
 $latRef  = $pengiriman->lat_penerima;
 $longRef = $pengiriman->long_penerima;
 if((Input::old('lat') != "") && (Input::old('long') != ""))
 {
   $latRef = Input::old('lat');
   $longRef= Input::old('long');
 }
 elseif(Input::has('lat') && Input::has('long'))
 {
   $latRef = Input::get('lat');
   $longRef= Input::get('long');
 }
 ?>
 @include('gmapjs.base', ['lat' => $latRef, 'lng' => $longRef])
 @include('gmapjs.probe-marker', ['parameter' => '#sPos', 'outLat' => '#sLat', 'outLong' => '#sLong'])
@endsection
@section('title')
  Konfirmasi Alamat
@endsection
@section('subtitle')
  <b>"Delivering your package nowhere"</b>
@endsection
@section('page')
 <div class="">
   <div id="map-canvas"></div>
   <form class="" action="{{ route('konfirmasi') }}" method="post">
     <select class="hidden" id="sPos">
       <option lat="{{ $pengiriman->lat_penerima }}" long="{{ $pengiriman->long_penerima }}">Penerima</option>
     </select>
     @include('templates.validation-base', ['parameter' => 'lat_penerima'])
     <div class="form-group">
       <label for="sLat">Latitude</label>
       <input type="text" name="lat_penerima" id="sLat" value="@include('templates.value-input-template', ['parameter' => 'lat_penerima'])" readonly class="form-control">
     </div>
     @include('templates.validation-base', ['parameter' => 'long_penerima'])
     <div class="form-group">
      <label for="sLong"></label>
      <input type="text" name="long_penerima" id="sLong" value="@include('templates.value-input-template', ['parameter' => 'long_penerima'])" readonly class="form-control">
     </div>
     <button type="submit" class="btn btn-success"><i class="ion ion-checkmark-circled"></i> Konfirmasi</button>
   </form>
 </div>
@endsection
