@extends('templates.backend-base')
@section('css')
 @parent

@endsection
@section('js')
 @parent

@endsection
@section('title')
  Dashboard
@endsection
@section('subtitle')
  Order Status
@endsection
@section('page')
@foreach($statuspengiriman as $status)
<div class="col-lg-3 col-xs-6">
  <div class="small-box {{ $status->boxclass }}">
    <div class="inner">
      <h3>{{ count($status->pengiriman) }}</h3>
      <p>{{ $status->status }}</p>
    </div>
    <div class="icon">
      <i class="ion ion-cube"></i>
    </div>
    <a href="{{ route('admin.status-pengiriman', $status->id) }}" class="small-box-footer">Rincian <i class="fa fa-arrow-circle-right"></i></a>
  </div>
</div>
@endforeach
@endsection
