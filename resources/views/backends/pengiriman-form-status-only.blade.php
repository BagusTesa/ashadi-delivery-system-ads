@extends('templates.backend-base')
@section('css')
  @parent
  <link rel="stylesheet" href="{{ asset('assets/css/form.css') }}">
@endsection
@section('js')
  @parent
  <script src="{{ asset('vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
  <script src="{{ asset('assets/js/order.js') }}"></script>
  <script src="{{ asset('assets/js/popup.js') }}"></script>
@endsection
@section('title')
Edit Paket
@endsection
@section('page')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Status Paket</h3>
  </div>
  <form id="orderForm" role="form" method="post" action="{{ route('admin.pengiriman.update', $pengiriman->id) }}">
    {!! csrf_field() !!}
    {!! method_field('PUT') !!}
    <div class="box-body">
      <div id="rootwizard">
        <ul>
          <li><a href="#tab1" data-toggle="tab">Pengiriman</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab1">
            <h4 class="form-title">Status</h4>
            <small><a href="#" onClick="PopupCenter('{{ route('admin.pengiriman.show', $pengiriman->token) }}', '123', '450', '620');">{{ $pengiriman->token }}</a></small>
            <hr>
            @include('templates.validation-base', ['parameter' => 'statuspengiriman_id'])
            <div class="form-group">
              <label for="sStatus_Pengiriman">Status</label>
              <select class="form-control required" id="sStatus_Pengiriman" name="statuspengiriman_id">
                @foreach($statuspengiriman as $s)
                <option value="{{ $s->id }}" @include('templates.value-option-template', ['parameter' => 'statuspengiriman_id', 'currId' => $s->id])>{{ $s->status }}</option>
                @endforeach
              </select>
            </div>
            <p><b>Perhatian</b>: mengubah status pengiriman selain <span class="label label-warning">tertunda</span> akan mengakibatkan pengiriman tidak dapat diubah pada kantor yang sama untuk keduakalinya.</p>
            <p>
                Untuk status <span class="label label-primary">Perjalanan</span> pengubahan status berikutnya terjadi pada kantor berikutnya melalui menu <b>Mobilitas Paket</b>.<br/>
                Sedangkan status <span class="label label-success">Terkirim</span> dan <span class="label label-danger">Gagal</span> tidak dapat diubah apapun yang terjadi.
            </p>
          </div>
          <hr>
          <div class="pull-right">
            <a class="btn btn-default" id="btn-next" href="#">Lanjut</a>
            <input type="submit" value="Simpan" class="btn btn-primary" id="btn-finish" style="display: none;" type="button">
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
