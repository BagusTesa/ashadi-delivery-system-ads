@extends('templates.backend-base')
@section('css')
  @parent
@endsection
@section('js')
  @parent
  <script src="{{ asset('assets/js/popup.js') }}"></script>
@endsection
@section('title')
Daftar Paket
@endsection
@section('page')
<div class="col-xs-12">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">List</h3>
      <div class="box-tools">
        <div class="input-group input-group-sm" style="width: 150px;">
          <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
          <div class="input-group-btn">
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        @include('templates.alert-base')
      </div>
    </div>
    <div class="box-body table-responsive no-padding">
      <table class="table table-hover">
        <tr>
          <th>Token</th>
          <th>Nama Pengirim</th>
          <th>Alamat Pengirim</th>
          <th>Nama Penerima</th>
          <th>Alamat Penerima</th>
          <th>Status</th>
          <th>Kota</th>
          <th></th>
          <th></th>
        </tr>
        @foreach($pengiriman as $p)
        <tr>
          <td><a href="#" onClick="PopupCenter('{{ route('admin.pengiriman.show', $p->token) }}', '123', '450', '620');">{{ str_limit($p->token, 10) }}</a></td>
          <td>{{ $p->nama_pengirim }}</td>
          <td>{{ str_limit($p->alamat_pengirim, 15) }}</td>
          <td>{{ $p->nama_penerima }}</td>
          <td>{{ str_limit($p->alamat_penerima, 15) }}</td>
          <td><span class="label {{ $p->status->labelclass }}">{{ $p->status->status }}</span></td>
          <td>{{ $p->kantor->kota->nama }}</td>
          <td class="align-center">
          @if(isset($editable) && $editable)
          <a href="{{ route('admin.pengiriman.edit', $p->token) }}"><button class="btn btn-warning"><i class="ion ion-edit"></i></button></a>
          @endif
          </td>
          <td class="align-center">
          @if(isset($deleteable) && $deleteable)
          <form method="post" action="{{ route('admin.pengiriman.destroy', $p->token) }}">{!! method_field('delete') !!}{!! csrf_field() !!}<button type="submit" class="btn btn-danger"><i class="ion ion-close-circled"></i></button></form>
          @endif
          </td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
  <div class="text-center">
    {!! $pengiriman->render() !!}
  </div>
</div>
@endsection
