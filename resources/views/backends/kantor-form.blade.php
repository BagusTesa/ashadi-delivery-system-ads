@extends('templates.backend-base')
@section('css')
  @parent
  <link rel="stylesheet" href="{{ asset('assets/css/form.css') }}">
@endsection
@section('js')
  @parent
  <script src="{{ asset('vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
  <?php
  $latRef  = $kota->first()->lat;
  $longRef = $kota->first()->long;
  if((Input::old('lat') != "") && (Input::old('long') != ""))
  {
    $latRef = Input::old('lat');
    $longRef= Input::old('long');
  }
  elseif(Input::has('lat') && Input::has('long'))
  {
    $latRef = Input::get('lat');
    $longRef= Input::get('long');
  }
  ?>
  @include('gmapjs.base', ['lat' => $latRef, 'lng' => $longRef])
  <script src="{{ asset('assets/js/order.js') }}"></script>
  @include('gmapjs.probe-marker', ['parameter' => '#sKota_id', 'outLat' => '#sLat', 'outLong' => '#sLong'])
@endsection
@section('title')
Tambah Kantor / Edit Kantor
@endsection
@section('page')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Rincian User</h3>
  </div>
  <form id="orderForm" role="form" method="post" action="@if(!Input::has('nama')){{ route('admin.kantordistribusi.store') }}@else{{ route('admin.kantordistribusi.update', $kantor->id) }}@endif">
    {!! csrf_field() !!}
    @if(Input::has('nama'))
    {!! method_field('PUT') !!}
    @endif
    <div class="box-body">
      <div id="rootwizard">
        <ul>
          <li><a href="#tab1" data-toggle="tab">Rincian</a></li>
          <li><a href="#tab2" data-toggle="tab">Posisi</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab1">
            <h4 class="form-title">Data Dasar</h4>
            <hr>
            @include('templates.validation-base', ['parameter' => 'nama'])
            <div class="form-group">
              <label for="sNama">Nama</label>
              <input type="text" name="nama" id="sNama" value="@include('templates.value-input-template', ['parameter' => 'nama'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['parameter' => 'alamat'])
            <div class="form-group">
              <label for="sAlamat">Alamat</label>
              <input type="text" name="alamat" id="sAlamat" value="@include('templates.value-input-template', ['parameter' => 'alamat'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['paramater' => 'telepon'])
            <div class="form-group">
              <label for="sTelepon">Telepon</label>
              <input type="text" name="telepon" id="sTelepon" value="@include('templates.value-input-template', ['parameter' => 'telepon'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['parameter' => 'kota_id'])
            <div class="form-group">
              <label for="sKota_id">Kota</label>
              <select class="form-control required" name="kota_id" id="sKota_id">
                @foreach($kota as $k)
                <option value="{{ $k->id }}" @include('templates.value-option-template', ['parameter' => 'kota_id', 'currId' => $k->id]) lat="{{ $k->lat }}" long="{{ $k->long }}">{{ $k->nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="tab-pane" id="tab2">
            <h4 class="form-title">Posisi Kantor</h4>
            <hr>
            <div class="row">
              <div class="col-md-8">
                <div class="" id="map-canvas"></div>
              </div>
              <div class="col-md-4">
                @include('templates.validation-base', ['parameter' => 'lat'])
                <div class="form-group">
                  <label for="sLat">Latitude</label>
                  <input type="text" name="lat" id="sLat" value="@include('templates.value-input-template', ['parameter' => 'lat'])" class="form-control required" readonly="true">
                </div>
                @include('templates.validation-base', ['parameter' => 'long'])
                <div class="form-group">
                  <label for="sLong">Longitude</label>
                  <input type="text" name="long" id="sLong" value="@include('templates.value-input-template', ['parameter' => 'long'])" class="form-control required" readonly="true">
                </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="pull-right">
            <a class="btn btn-default" id="btn-next" href="#">Lanjut</a>
            <input type="submit" value="Simpan" class="btn btn-primary" id="btn-finish" style="display: none;" type="button">
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
