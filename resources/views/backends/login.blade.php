@extends('templates.master-base')
@section('css')
  @parent
  <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
  <link rel="stylesheet" href="{{ 'vendor/AdminLTE/dist/css/AdminLTE.min.css' }}">
@endsection
@section('title'){{ 'Login' }}@endsection
@section('body-class'){{ 'hold-transition login-page' }}@endsection
@section('body')
<div class="background-image"></div>
<div class="login-box">
  <div class="login-logo">
    <b>Ashadi</b> Delivery System <small>v0.0.1</small>
  </div>
  <div class="login-box-body">
    @include('templates.alert-base')
    <p class="login-box-msg">Silahkan masuk untuk mengakses administrasi Ashadi Delivery</p>
    <form action="{{ route('login') }}" method="post">
      {!! csrf_field() !!}
      @include('templates.validation-base', ['parameter' => 'email'])
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="E-mail" name="email" value="{{ Input::old('email') }}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      @include('templates.validation-base', ['parameter' => 'password'])
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <!-- <a href="#">Lupa Password</a><br> -->
        </div>
        <div class="col-xs-4">
          <input type="submit" value="Sign In" class="btn btn-primary btn-block btn-flat">
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
