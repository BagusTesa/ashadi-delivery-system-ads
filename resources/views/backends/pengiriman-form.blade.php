@extends('templates.backend-base')
@section('css')
  @parent
  <link rel="stylesheet" href="{{ asset('assets/css/form.css') }}">
@endsection
@section('js')
  @parent
  <script src="{{ asset('vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
  @include('gmapjs.base', ['lat' => $kota->first()->lat, 'lng' => $kota->first()->long])
  <script src="{{ asset('assets/js/order.js') }}"></script>
  @include('gmapjs.probe-marker', ['parameter' => '#sKota_Penerima', 'outLat' => '#sLat_Penerima', 'outLong' => '#sLong_Penerima'])
@endsection
@section('title')
Paket Baru
@endsection
@section('page')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Rincian Paket</h3>
  </div>
  <form id="orderForm" role="form" method="post" action="@if(!Input::has('nama_pengirim')){{ route('admin.pengiriman.store') }}@else{{ route('admin.pengiriman.update', $pengiriman->token) }}@endif">
    {!! csrf_field() !!}
    @if(Input::has('nama_pengirim'))
    {!! method_field('PUT') !!}
    @endif
    <div class="box-body">
      <div id="rootwizard">
        <ul>
          <li><a href="#tab1" data-toggle="tab">Pengirim</a></li>
          <li><a href="#tab2" data-toggle="tab">Penerima</a></li>
          <li><a href="#tab3" data-toggle="tab">Barang</a></li>
          <li><a href="#tab4" data-toggle="tab">Peta</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab1">
            <h4 class="form-title">Data Pengirim Paket</h4>
            <hr>
            @include('templates.validation-base', ['parameter' => 'nama_pengirim'])
            <div class="form-group">
              <label for="sNamaPengirim">Nama</label>
              <input type="text" name="nama_pengirim" id="sNamaPengirim" value="@include('templates.value-input-template', ['parameter' => 'nama_pengirim'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['parameter' => 'alamat_pengirim'])
            <div class="form-group">
              <label for="sAlamatPengirim">Alamat</label>
              <input type="text" name="alamat_pengirim" id="sAlamatPengirim" value="@include('templates.value-input-template', ['parameter' => 'alamat_pengirim'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['paramater' => 'telepon_pengirim'])
            <div class="form-group">
              <label for="sTeleponPengirim">Telepon</label>
              <input type="text" name="telepon_pengirim" id="sTeleponPengirim" value="@include('templates.value-input-template', ['parameter' => 'telepon_pengirim'])" class="form-control required">
            </div>
          </div>
          <div class="tab-pane" id="tab2">
            <h4 class="form-title">Data Penerima Paket</h4>
            <hr>
            @include('templates.validation-base', ['parameter' => 'nama_penerima'])
            <div class="form-group">
              <label for="sNamaPenerima">Nama</label>
              <input type="text" name="nama_penerima" id="sNamaPenerima" value="@include('templates.value-input-template', ['parameter' => 'nama_penerima'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['parameter' => 'alamat_penerima'])
            <div class="form-group">
              <label for="sAlamatPenerima">Alamat</label>
              <input type="text" name="alamat_penerima" id="sAlamatPenerima" value="@include('templates.value-input-template', ['parameter' => 'alamat_penerima'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['parameter' => 'kota_penerima'])
            <div class="form-group">
              <label for="sKota_Penerima">Kota</label>
              <select class="form-control required" name="kota_penerima" id="sKota_Penerima">
                @foreach($kota as $k)
                <option value="{{ $k->id }}" @include('templates.value-option-template', ['parameter' => 'kota_penerima', 'currId' => $k->id]) lat={{ $k->lat }} long={{ $k->long }}>{{ $k->nama }}</option>
                @endforeach
              </select>
            </div>
            @include('templates.validation-base', ['paramater' => 'telepon_penerima'])
            <div class="form-group">
              <label for="sTeleponPenerima">Telepon</label>
              <input type="text" name="telepon_penerima" id="sTeleponPenerima" value="@include('templates.value-input-template', ['parameter' => 'telepon_penerima'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['paramater' => 'email_penerima'])
            <div class="form-group">
              <label for="sEmailPenerima">E-mail</label>
              <input type="text" name="email_penerima" id="sEmailPenerima" value="@include('templates.value-input-template', ['parameter' => 'email_penerima'])" class="form-control required">
            </div>
          </div>
          <div class="tab-pane" id="tab3">
            <h4 class="form-title">Data Barang</h4>
            <hr>
            @include('templates.validation-base', ['parameter' => 'volume_barang'])
            <div class="form-group">
              <label for="sVolumeBarang">Volume</label>
              <input type="text" name="volume_barang" id="sVolumeBarang" value="@include('templates.value-input-template', ['parameter' => 'volume_barang'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['parameter' => 'berat_barang'])
            <div class="form-group">
              <label for="sBeratBarang">Berat</label>
              <input type="text" name="berat_barang" id="sBeratBarang" value="@include('templates.value-input-template', ['parameter' => 'berat_barang'])" class="form-control required">
            </div>
          </div>
          <div class="tab-pane" id="tab4">
            <div class="row">
              <div class="col-md-8">
                <div class="" id="map-canvas"></div>
              </div>
              <div class="col-md-4">
                @include('templates.validation-base', ['parameter' => 'lat_penerima'])
                <div class="form-group">
                  <label for="sLat_Penerima">Latitude</label>
                  <input type="text" name="lat_penerima" id="sLat_Penerima" value="@include('templates.value-input-template', ['parameter' => 'lat_penerima'])" class="form-control required" readonly="true">
                </div>
                @include('templates.validation-base', ['parameter' => 'long_penerima'])
                <div class="form-group">
                  <label for="sLong_Penerima">Longitude</label>
                  <input type="text" name="long_penerima" id="sLong_Penerima" value="@include('templates.value-input-template', ['parameter' => 'long_penerima'])" class="form-control required" readonly="true">
                </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="pull-right">
            <a class="btn btn-default" id="btn-next" href="#">Lanjut</a>
            <input type="submit" value="Simpan" class="btn btn-primary" id="btn-finish" style="display: none;" type="button">
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
