@extends('templates.backend-base')
@section('css')
  @parent
@endsection
@section('js')
  @parent
  <script src="{{ asset('assets/js/popup.js') }}"></script>
@endsection
@section('title')
Daftar Kantor Distribusi
@endsection
@section('page')
<div class="col-xs-12">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">List</h3>
      <div class="box-tools">
        <div class="input-group input-group-sm" style="width: 150px;">
          <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
          <div class="input-group-btn">
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        @include('templates.alert-base')
      </div>
    </div>
    <div class="box-body table-responsive no-padding">
      <table class="table table-hover">
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Alamat</th>
          <th>Kota</th>
          <th>Lat</th>
          <th>Long</th>
          <th></th>
          <th></th>
        </tr>
        <?php $point = 0 + ($kantor->currentPage() - 1) * $kantor->perPage(); ?>
        @foreach($kantor as $k)
        <tr>
          <td>{{ ++$point }}</td>
          <td><a href="#" onClick="PopupCenter('{{ route('admin.kantordistribusi.show', $k->id) }}', '123', '450', '620');">{{ $k->nama }}</a></td>
          <td>{{ $k->alamat }}</td>
          <td>{{ $k->kota->nama }}</td>
          <td>{{ $k->lat }}</td>
          <td>{{ $k->long }}</td>
          <td class="align-center">@if($k->kota != null)<a href="{{ route('admin.kantordistribusi.edit', $k->id) }}"><button class="btn btn-warning"><i class="ion ion-edit"></i></button></a>@endif</td>
          <td class="align-center">@if($k->kota != null)<form method="post" action="{{ route('admin.kantordistribusi.destroy', $k->id) }}">{!! method_field('delete') !!}{!! csrf_field() !!}<button type="submit" class="btn btn-danger"><i class="ion ion-close-circled"></i></button></form>@endif</td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
  <div class="text-center">
    {!! $kantor->render() !!}
  </div>
</div>
@endsection
