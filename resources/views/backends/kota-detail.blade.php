@extends('templates.popup-base')
@section('css')
 @parent

@endsection
@section('js')
 @parent
 @include('gmapjs.base', ['lat' => $kota->lat, 'lng' => $kota->long])
 @include('gmapjs.probe-marker', ['parameter' => "#ref"])
@endsection
@section('title')
  Rincian Kota
@endsection
@section('subtitle')
  Kota: {{ $kota->nama }}
@endsection
@section('page')
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Informasi Dasar</h3>
    </div>
    <div class="box-body">
      <dl>
        <dt>Nama Kota</dt>
        <dd>{{ $kota->nama }}</dd>
        <dt>Jumlah Kantor</dt>
        <dd>{{ count($kota->kantorDistribusi) }}</dd>
        <dt>Latitude</dt>
        <dd>{{ $kota->lat }}</dd>
        <dt>Longitude</dt>
        <dd>{{ $kota->long }}</dd>
      </dl>
    </div>
  </div>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Informasi Pergerakan Paket</h3>
    </div>
    <div class="box-body">
      <dl>
        <dt>Pengiriman Bertujuan</dt>
        <dd>{{ count($kota->pengirimanTujuan) }}</dd>
        <dt>Pengiriman Berasal</dt>
        <dd>{{ count($kota->pengirimanBerasal) }}</dd>
      </dl>
    </div>
  </div>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Posisi Peta</h3>
    </div>
    <div class="box-body">
      <select class="hidden" id="ref">
        <option selected lat="{{ $kota->lat }}" long="{{ $kota->long }}"></option>
      </select>
      <div id="map-canvas">
        google map
      </div>
    </div>
  </div>
</section>
@endsection
