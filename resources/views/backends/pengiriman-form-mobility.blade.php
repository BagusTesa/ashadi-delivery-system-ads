@extends('templates.backend-base')
@section('css')
  @parent
  <link rel="stylesheet" href="{{ asset('assets/css/form.css') }}">
@endsection
@section('js')
  @parent
  <script src="{{ asset('vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
  <script src="{{ asset('assets/js/order.js') }}"></script>
  <script src="{{ asset('assets/js/popup.js') }}"></script>
@endsection
@section('title')
{{ $title }}
@endsection
@section('page')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">@yield('title')</h3>
  </div>
  <form id="orderForm" role="form" method="post" action="{{ $postRoute }}">
    {!! csrf_field() !!}
    <div class="box-body">
      <div id="rootwizard">
        <ul>
          <li><a href="#tab1" data-toggle="tab">Paket Masuk</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab1">
            <h4 class="form-title">Status</h4>
            <hr>
            @include('templates.validation-base', ['parameter' => 'token'])
            <div class="form-group">
              <label for="sToken">Token</label>
              <input type="text" name="token" id="sToken" value="@include('templates.value-input-template', ['parameter' => 'token'])" class="form-control required">
            </div>
            @if($postRoute == route('admin.postpaketmasuk'))
            <p>
              Halaman ini hanya menerima <b>paket</b> yang memiliki status <span class="label label-primary">perjalanan</span> dan tidak terletak pada kantor ini.<br/>
            </p>
            @elseif($postRoute == route('admin.postpaketkeluar'))
            <p>
              Halaman ini hanya menerima <b>paket</b> yang memiliki status <span class="label label-warning">tertunda</span> dan terletak pada kantor ini.<br/>
            </p>
            @endif
          </div>
          <hr>
          <div class="pull-right">
            <a class="btn btn-default" id="btn-next" href="#">Lanjut</a>
            <input type="submit" value="Simpan" class="btn btn-primary" id="btn-finish" style="display: none;" type="button">
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
