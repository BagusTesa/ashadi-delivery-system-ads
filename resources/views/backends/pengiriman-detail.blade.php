@extends('templates.popup-base')
@section('css')
 @parent

@endsection
@section('js')
 @parent
 @if(isset($pengiriman->lat_penerima) && isset($pengiriman->long_penerima))
   @include('gmapjs.base', ['lat' => $pengiriman->lat_penerima, 'lng' => $pengiriman->long_penerima])
   @if(isset($directions) && $directions)
    @include('gmapjs.direction', [
      'startName' => $pengiriman->kantor->nama,
      'startLat'  => $pengiriman->kantor->lat,
      'startLng'  => $pengiriman->kantor->long,
      'endName'   => $pengiriman->nama_penerima,
      'endLat'    => $pengiriman->lat_penerima,
      'endLng'    => $pengiriman->long_penerima
    ])
   @else
     @include('gmapjs.probe-marker', ['parameter' => '#ref'])
   @endif
 @endif
@endsection
@section('title')
  Rincian Pengiriman
@endsection
@section('subtitle')
  ID: {{ $pengiriman->token }}
@endsection
@section('page')
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Informasi Dasar</h3>
    </div>
    <div class="box-body">
      <dl>
        <dt>Kota Sekarang</dt>
        <dd>{{ $pengiriman->kantor->kota->nama }} ({{ $pengiriman->kantor->nama }})</dd>
        <dt>Status</dt>
        <dd><span class="label {{ $pengiriman->status->labelclass }}">{{ $pengiriman->status->status }}</span></dd>
        <dt>Volume</dt>
        <dd>{{ $pengiriman->volume_barang }}</dd>
        <dt>Berat</dt>
        <dd>{{ $pengiriman->berat_barang }}</dd>
        <dt>Tanggal Dikirimkan</dt>
        <dd>{{ $pengiriman->created_at }}</dd>
        <dt>Tanggal Pergerakan Terakhir</dt>
        <dd>{{ $pengiriman->updated_at }}</dd>
      </dl>
    </div>
  </div>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Informasi Pengirim</h3>
    </div>
    <div class="box-body">
      <dl>
        <dt>Nama</dt>
        <dd>{{ $pengiriman->nama_pengirim }}</dd>
        <dt>Alamat</dt>
        <dd>{{ $pengiriman->alamat_pengirim }}</dd>
        <dt>Kota</dt>
        <dd>{{ $pengiriman->asal->nama }}</dd>
        <dt>Telepon</dt>
        <dd>{{ $pengiriman->telepon_pengirim }}</dd>
      </dl>
    </div>
  </div>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Informasi Penerima</h3>
    </div>
    <div class="box-body">
      <dl>
        <dt>Nama</dt>
        <dd>{{ $pengiriman->nama_penerima }}</dd>
        <dt>Alamat</dt>
        <dd>{{ $pengiriman->alamat_penerima }}</dd>
        <dt>Kota</dt>
        <dd>{{ $pengiriman->tujuan->nama }}</dd>
        <dt>Telepon</dt>
        <dd>{{ $pengiriman->telepon_penerima }}</dd>
        @if(isset($pengiriman->lat_penerima) && isset($pengiriman->long_penerima))
        <dt>Lokasi</dt>
        <select id="ref" class="hidden">
          <option selected lat="{{ $pengiriman->lat_penerima }}" long="{{ $pengiriman->long_penerima }}">{{ $pengiriman->token }}</option>
        </select>
        <div id="map-canvas">
          google map
        </div>
        @endif
      </dl>
    </div>
  </div>
</section>
@endsection
