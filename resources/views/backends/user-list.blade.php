@extends('templates.backend-base')
@section('css')
  @parent
@endsection
@section('js')
  @parent
  <script src="{{ asset('assets/js/popup.js') }}"></script>
@endsection
@section('title')
Daftar User
@endsection
@section('page')
<div class="col-xs-12">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">List</h3>
      <div class="box-tools">
        <div class="input-group input-group-sm" style="width: 150px;">
          <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
          <div class="input-group-btn">
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        @include('templates.alert-base')
      </div>
    </div>
    <div class="box-body table-responsive no-padding">
      <table class="table table-hover">
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>E-mail</th>
          <th>Kantor</th>
          <th></th>
          <th></th>
        </tr>
        <?php $point = 0 + ($user->currentPage() - 1) * $user->perPage(); ?>
        @foreach($user as $u)
        <tr>
          <td>{{ ++$point }}</td>
          <td>{{ $u->name }}</td>
          <td>{{ $u->email }}</td>
          <td>@if(($u->kantor != null) && ($u->kantor->kota != null)){{ $u->kantor->nama . ' (' . $u->kantor->kota->nama . ')' }}@else{!! '<span class="label label-warning">Admin Pusat</span>' !!}@endif</td>
          <td class="align-center">@if($u->kantor != null)<a href="{{ route('admin.user.edit', $u->id) }}"><button class="btn btn-warning"><i class="ion ion-edit"></i></button></a>@endif</td>
          <td class="align-center">@if($u->kantor != null)<form method="post" action="{{ route('admin.user.destroy', $u->id) }}">{!! method_field('delete') !!}{!! csrf_field() !!}<button type="submit" class="btn btn-danger"><i class="ion ion-close-circled"></i></button></form>@endif</td>
        </tr>
        @endforeach
      </table>
    </div>
  </div>
  <div class="text-center">
    {!! $user->render() !!}
  </div>
</div>
@endsection
