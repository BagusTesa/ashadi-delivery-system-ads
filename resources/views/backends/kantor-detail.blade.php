@extends('templates.popup-base')
@section('css')
 @parent

@endsection
@section('js')
 @parent
 @include('gmapjs.base', ['lat' => $kantor->lat, 'lng' => $kantor->long])
 @include('gmapjs.probe-marker', ['parameter' => "#ref"])
@endsection
@section('title')
  Rincian Kantor Distribusi
@endsection
@section('subtitle')
  Kantor: {{ $kantor->nama }}
@endsection
@section('page')
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Informasi Dasar</h3>
    </div>
    <div class="box-body">
      <dl>
        <dt>Nama Kantor</dt>
        <dd>{{ $kantor->nama }}</dd>
        <dt>Jumlah User</dt>
        <dd>{{ count($kantor->user) }}</dd>
        <dt>Latitude</dt>
        <dd>{{ $kantor->lat }}</dd>
        <dt>Longitude</dt>
        <dd>{{ $kantor->long }}</dd>
      </dl>
    </div>
  </div>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Data Kota</h3>
    </div>
    <div class="box-body">
      <dl>
        <dt>Nama Kota</dt>
        <dd>{{ $kantor->kota->nama }}</dd>
        <dt>Jumlah Kantor</dt>
        <dd>{{ count($kantor->kota->KantorDistribusi) }}</dd>
        <dt>Latitude</dt>
        <dd>{{ $kantor->lat }}</dd>
        <dt>Longitude</dt>
        <dd>{{ $kantor->long }}</dd>
      </dl>
    </div>
  </div>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Posisi Peta</h3>
    </div>
    <div class="box-body">
      <select class="hidden" id="ref">
        <option selected lat="{{ $kantor->lat }}" long="{{ $kantor->long }}"></option>
      </select>
      <div id="map-canvas">
        google map
      </div>
    </div>
  </div>
</section>
@endsection
