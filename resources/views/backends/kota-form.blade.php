@extends('templates.backend-base')
@section('css')
  @parent
  <link rel="stylesheet" href="{{ asset('assets/css/form.css') }}">
@endsection
@section('js')
  @parent
  <script src="{{ asset('vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
  <?php
  $latRef  = $kota->first()->lat;
  $longRef = $kota->first()->long;
  if((Input::old('lat') != "") && (Input::old('long') != ""))
  {
    $latRef = Input::old('lat');
    $longRef= Input::old('long');
  }
  elseif(Input::has('lat') && Input::has('long'))
  {
    $latRef = Input::get('lat');
    $longRef= Input::get('long');
  }
  ?>
  @include('gmapjs.base', ['lat' => $latRef, 'lng' => $longRef])
  <script src="{{ asset('assets/js/order.js') }}"></script>
  @include('gmapjs.geocoder', ['parameter' => '#sNama', 'trigger' => '#sTrigger','outLat' => '#sLat', 'outLong' => '#sLong', 'lat' => $latRef, 'lng' => $longRef])
@endsection
@section('title')
Tambah Kantor / Edit Kantor
@endsection
@section('page')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Rincian User</h3>
  </div>
  <form id="orderForm" role="form" method="post" action="@if(!Input::has('nama')){{ route('admin.kota.store') }}@else{{ route('admin.kota.update', $kota->id) }}@endif">
    {!! csrf_field() !!}
    @if(Input::has('nama'))
    {!! method_field('PUT') !!}
    @endif
    <div class="box-body">
      <div id="rootwizard">
        <ul>
          <li><a href="#tab1" data-toggle="tab">Rincian</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab1">
            <h4 class="form-title">Data Dasar</h4>
            <hr>
            @include('templates.validation-base', ['parameter' => 'nama'])
            <div class="form-group">
              <label for="sNama">Nama</label>
              <div class="input-group">
                <input type="text" name="nama" id="sNama" value="@include('templates.value-input-template', ['parameter' => 'nama'])" class="form-control required">
                <span class="input-group-btn">
                  <button type="button" id="sTrigger" class="btn btn-primary"><i class="ion ion-ios-search-strong"></i> Temukan</button>
                </pan>
              </div>
            </div>
            <div class="form-group">
              <div id="map-canvas"></div>
            </div>
            @include('templates.validation-base', ['parameter' => 'lat'])
            <div class="form-group">
              <label for="sLat">Latitude</label>
              <input type="text" name="lat" id="sLat" value="@include('templates.value-input-template', ['parameter' => 'lat'])" class="form-control required" readonly="true">
            </div>
            @include('templates.validation-base', ['parameter' => 'long'])
            <div class="form-group">
              <label for="sLong">Longitude</label>
              <input type="text" name="long" id="sLong" value="@include('templates.value-input-template', ['parameter' => 'long'])" class="form-control required" readonly="true">
            </div>
          </div>
          <hr>
          <div class="pull-right">
            <a class="btn btn-default" id="btn-next" href="#">Lanjut</a>
            <input type="submit" value="Simpan" class="btn btn-primary" id="btn-finish" style="display: none;" type="button">
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
