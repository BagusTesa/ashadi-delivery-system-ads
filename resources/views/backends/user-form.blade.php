@extends('templates.backend-base')
@section('css')
  @parent
  <link rel="stylesheet" href="assets/css/form.css">
@endsection
@section('js')
  @parent
  <script src="{{ asset('vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('vendor/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
  <script src="{{ asset('assets/js/order.js') }}"></script>
@endsection
@section('title')
Tambah User / Edit User
@endsection
@section('page')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Rincian User</h3>
  </div>
  <form id="orderForm" role="form" method="post" action="@if(!Input::has('name')){{ route('admin.user.store') }}@else{{ route('admin.user.update', $user->id) }}@endif">
    {!! csrf_field() !!}
    @if(Input::has('name'))
    {!! method_field('PUT') !!}
    @endif
    <div class="box-body">
      <div id="rootwizard">
        <ul>
          <li><a href="#tab1" data-toggle="tab">Rincian</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab1">
            <h4 class="form-title">Data Dasar</h4>
            <hr>
            @include('templates.validation-base', ['parameter' => 'name'])
            <div class="form-group">
              <label for="sName">Nama</label>
              <input type="text" name="name" id="sName" value="@include('templates.value-input-template', ['parameter' => 'name'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['parameter' => 'email'])
            <div class="form-group">
              <label for="sEmail">E-mail</label>
              <input type="email" name="email" id="sEmail" value="@include('templates.value-input-template', ['parameter' => 'email'])" class="form-control required">
            </div>
            @include('templates.validation-base', ['parameter' => 'password'])
            @if(Input::has('name'))
            <div class="form-group">
              <label for="sold_Password">Old Password</label>
              <input type="password" name="old_password" id="sold_Password" class="form-control">
            </div>
            @endif
            <div class="form-group">
              <label for="sPassword">Password</label>
              <input type="password" name="password" id="sPassword" class="form-control @if(!Input::has('name')){{ 'required' }}@endif">
            </div>
            <div class="form-group">
              <label for="sPasswordConfirmed">Konfirmasi Password</label>
              <input type="password" name="password_confirmation" value="" class="form-control @if(!Input::has('name')){{ 'required' }}@endif">
            </div>
            @include('templates.validation-base', ['parameter' => 'kantor_id'])
            <div class="form-group">
              <label for="sKantor">Kantor</label>
              <select class="form-control required" name="kantor_id" id="sKantor">
                @foreach($kantor as $k)
                <option value="{{ $k->id }}" @include('templates.value-option-template', ['parameter' => 'kantor_id', 'currId' => $k->id])>{{ $k->nama }} ({{ $k->kota->nama }})</option>
                @endforeach
              </select>
            </div>
          </div>
          <hr>
          <div class="pull-right">
            <input type="submit" value="Simpan" class="btn btn-primary" id="btn-finish" style="display: none;" type="button">
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
