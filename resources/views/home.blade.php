<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map-canvas {
        height: 100%;
      }
    </style>
    <title>Trial Map</title>
  </head>
  <body>
    <div id="map-canvas"></div>
    @include('gmapjs.base', ['lat' => -34.397, 'lng' => 150.644])
  </body>
</html>
