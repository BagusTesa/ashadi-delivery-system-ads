<?php

return [
    'login'=> [
      'success'=> 'login berhasil',
      'failed' => 'login gagal',
    ],
    'crud' => [
      'creation' => [
        'success' => ':item berhasil dibuat',
        'failed'  => ':item gagal dibuat',
      ],
      'update' => [
        'success' => ':item berhasil diperbarui',
        'failed'  => ':item gagal diperbarui',
      ],
      'delete' => [
        'success' => ':item berhasil dihilangkan',
        'failed'  => ':item gagal dihilangkan',
      ],
    ],
    'map' => [
      'success' => 'peta berhasil berhasil hore',
      'failed'  => 'peta gagal dimuat',
    ],
];
