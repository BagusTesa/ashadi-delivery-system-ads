<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute harus diterima.',
    'active_url'           => ':attribute bukan merupakan URL yang valid.',
    'after'                => ':attribute harus tanggal setelah tanggal :date.',
    'alpha'                => ':attribute sebaiknya berisi huruf saja.',
    'alpha_dash'           => ':attribute sebaiknya berisi huruf,angka, dan dashes saja.',
    'alpha_num'            => ':attribute sebaiknya berisi huruf dan angka saja.',
    'array'                => ':attribute harus berupa array.',
    'before'               => ':attribute harus tanggal sebelum tanggal :date.',
    'between'              => [
        'numeric' => ':attribute harus diantara :min dan :max.',
        'file'    => ':attribute harus diantara :min dan :max kilobytes.',
        'string'  => ':attribute harus diantara :min dan :max karakter.',
        'array'   => ':attribute harus memiliki barang berjumlah diantara :min dan :max.',
    ],
    'boolean'              => 'bagian :attribute harus true atau false.',
    'confirmed'            => ':attribute konfirmasi tidak cocok.',
    'date'                 => ':attribute bukan merupakan tanggal yang valid.',
    'date_format'          => ':attribute tidak cocok dengan format format :format.',
    'different'            => ':attribute dan :other harus berbeda.',
    'digits'               => ':attribute harus :digits digit.',
    'digits_between'       => ':attribute harus diantar :min dan :max digit.',
    'email'                => ':attribute harus berupa alamat email yang valid.',
    'exists'               => ':attribute yang dipilih tidak valid.',
    'filled'               => 'bagian :attribute dibutuhkan.',
    'image'                => ':attribute harus berupa gambar.',
    'in'                   => ':attribute yang dipilih tidak valid.',
    'integer'              => ':attribute harus berupa integer.',
    'ip'                   => ':attribute harus berupa IP address yang valid.',
    'json'                 => ':attribute harus berupa JSON string yang valid.',
    'max'                  => [
        'numeric' => ':attribute sebaiknya tidak lebih dari :max.',
        'file'    => ':attribute sebaiknya tidak lebih dari :max kilobytes.',
        'string'  => ':attribute sebaiknya tidak lebih dari :max karakter.',
        'array'   => ':attribute sebaiknya tidak memiliki lebih dari :max barang.',
    ],
    'mimes'                => ':attribute harus lah file bertipe: :values.',
    'min'                  => [
        'numeric' => ':attribute harus minimal :min.',
        'file'    => ':attribute harus minimal :min kilobytes.',
        'string'  => ':attribute harus minimal :min karakter.',
        'array'   => ':attribute harus memiliki minimal :min barang.',
    ],
    'not_in'               => ':attribute yang dipilih tidak valid.',
    'numeric'              => ':attribute harus angka.',
    'regex'                => ':attribute format tidak valid.',
    'required'             => 'bagian :attribute dibutuhkan.',
    'required_if'          => 'bagian :attribute dibutuhkan saat :other adalah :value.',
    'required_with'        => 'bagian :attribute dibutuhkan saat :values muncul.',
    'required_with_all'    => 'bagian :attribute dibutuhkan saat :values muncul.',
    'required_without'     => 'bagian :attribute dibutuhkan saat :values tidak muncul.',
    'required_without_all' => 'bagian :attribute dibutuhkan saat tidak ada :values yang muncul.',
    'same'                 => ':attribute dan :other harus cocok.',
    'size'                 => [
        'numeric' => ':attribute harus :size.',
        'file'    => ':attribute harus :size kilobytes.',
        'string'  => ':attribute harus :size characters.',
        'array'   => ':attribute harus berisi :size barang.',
    ],
    'string'               => ':attribute harus string.',
    'timezone'             => ':attribute harus zona yang valid.',
    'unique'               => ':attribute telah diambil.',
    'url'                  => ':attribute format tidak valid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'password' => [
            'old' => 'Kata sandi lama salah',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
