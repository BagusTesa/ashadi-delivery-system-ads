<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Kata sandi minimal enam karakter dan sesuai dengan konfirmasi.',
    'reset' => 'Kata sandi anda telah diatur ulang!',
    'sent' => 'Tautan untuk mengatur ulang kata sandi telah dikirim ke e-mail anda!',
    'token' => 'Token pengaturan ulang kata sandi sudah tidak valid.',
    'user' => "E-mail tidak ditemukan.",

];
