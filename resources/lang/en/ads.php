<?php

return [
    'login'=> [
      'success'=> 'yorokobe shounen',
      'failed' => 'Login failed',
    ],
    'crud' => [
      'creation' => [
        'success' => ':item successfuly created',
        'failed'  => ':item failed to be created',
      ],
      'update' => [
        'success' => ':item successfuly updated',
        'failed'  => ':item failed to be updated',
      ],
      'delete' => [
        'success' => ':item successfuly removed',
        'failed'  => ':item failed to be removed',
      ],
    ],
    'map' => [
      'success' => 'Abura de ageta pan',
      'failed'  => 'map fails, do not ask why',
    ],
];
