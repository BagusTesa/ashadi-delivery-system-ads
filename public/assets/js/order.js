$(document).ready(function() {
  var $validator = $('#orderForm').validate({
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });

  $('#rootwizard').bootstrapWizard({
    'tabClass': 'nav nav-tabs',
    'nextSelector': '#btn-next',
    'lastSelector': '#btn-finish',
    'onTabClick': function(tab, navigation, index) {
      return false;
    },
    'onNext': function(tab, navigation, index) {
      var $valid = $("#orderForm").valid();
      if(!$valid) {
        $validator.focusInvalid();
        return false;
      }
    },
    'onTabShow': function(tab, navigation, index) {
      var $total = navigation.find('li').length;
      var $current = index+1;
      if($current >= $total) {
        $('#rootwizard').find('#btn-next').hide();
        $('#rootwizard').find('#btn-finish').show();
      } else {
        $('#rootwizard').find('#btn-next').show();
        $('#rootwizard').find('#btn-finish').hide();
      }
      google.maps.event.trigger(map, 'resize');
    }
  });
});
