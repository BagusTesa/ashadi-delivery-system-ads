(function(tgmap, $, undefined){
  //assume only single map
  var map         = null;
  var mapOptions  = null;
  var mapMarker   = [];
  var isGmapReady = false;
  tgmap.createLatLngLiteral(_lat, _lng){
    return {lat: _lat, lng: _lng};
  }
  tgmap.createMarker(_lat, _lng, _title, _draggable){
    var mark = new google.maps.marker({
      position: tgmap.createLatLngLiteral(_lat, _lng),
      map: map,
      title: _title,
      draggable: _draggable
    });
    return mapMarker.push(mark);
  }
  tgmap.setMapOptions(_zoom, _lat, _lng){
    return {zoom: _zoom, center: tgmap.createLatLngLiteral(_lat, _lng)};
  }
  tgmap.initializeMap(div){
    div = div || "#map-canvas";
    map = new google.maps.Map($(div).get[0], mapOptions)
    isGmapReady = true;
  }

  tgmap.addMarkerPicker(marker_id, _latField, _lngField){
    google.maps.event.addListener(mapMarker[marker_id], 'dragend', function(){
      
    })
  }

}(window.tgmap = window.tgmap || {} jQuery));
