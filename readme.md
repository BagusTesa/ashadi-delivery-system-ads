## Ashadi Delivery System

A self-proclaimed delivery system that mimics common delivery system built atop [Laravel](http://laravel.com/) and several packages to do the job.

**NOTE:**

This project is not a full-fledged delivery information system - just a mock up done for [this](http://elisa.ugm.ac.id/community/show/sistem-informasi-geografis-arif-n/).

**Requirement:**

* [Composer](https://getcomposer.org/download/) - so you'll be forced to register your `php.exe` to environment variable (especially Windows)
* [All Laravel Requirement](http://laravel.com/docs/5.1/)
* Database (suggested MariaDB/MySQL - with PhpMyAdmin/MySQL Workbench)
* Internet Connection, for seeding and GoogleMap - thus [GoogleMap requirement](https://support.google.com/mymaps/answer/3034273?hl=en) also required
* [FakeSMTP](https://nilhcem.github.io/FakeSMTP/) if you enabled mailing

To make things, easier you could use [XAMPP](https://www.apachefriends.org/index.html) application stack for simplicity.

**Setting Up:**

* Start database server
* Create a database
* Save as `.env.example` as `.env`
* Configure `.env` database credentials (obligatory) and SMTP credentials (if you want to enable mailling)
* Generate application key for this project,  simply open a command prompt, point to this project directory, then.. it happens
```
php artisan key:generate
```
* Migrate - create table - that used by this project.
```
php artisan migrate:refresh --seed
```
* Start HTTP server (for XAMPP user, that'll be Apache)
* Open the project's public directory with the chosen browser
