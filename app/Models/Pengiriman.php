<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
  protected $table       = "pengiriman";
  protected $fillable    = ['nama_pengirim',
                            'alamat_pengirim',
                            'kota_pengirim',
                            'telepon_pengirim',
                            'nama_penerima',
                            'alamat_penerima',
                            'kota_penerima',
                            'telepon_penerima',
                            'email_penerima',
                            'lat_penerima',
                            'long_penerima',
                            'volume_barang',
                            'berat_barang',
                           ];
  protected $dates       = ['created_at', 'updated_at'];
  protected $casts       = ['lat'             => 'double',
                            'long'            => 'double',
                            'volume_barang'   => 'double',
                            'berat_barang'    => 'double',
                            'emailkonfirmasi' => 'boolean',];
  public $primaryKey     = "token";
  public $timestamps     = true;
  public $incrementing   = false;

  public function asal()
  {
    return $this->belongsTo('App\Models\Kota', 'kota_pengirim', 'id');
  }

  public function tujuan()
  {
    return $this->belongsTo('App\Models\Kota', 'kota_penerima', 'id');
  }

  public function kantor()
  {
    return $this->belongsTo('App\Models\KantorDistribusi', 'kantor_id', 'id');
  }

  public function status()
  {
    return $this->belongsTo('App\Models\StatusPengiriman', 'statuspengiriman_id', 'id');
  }

  public function setNamaPengirimAttribute($value)
  {
    $this->attributes['nama_pengirim'] = ucwords($value);
  }

  public function setAlamatPengirimAttribute($value)
  {
    $this->attributes['alamat_pengirim'] = strtolower($value);
  }

  public function setNamaPenerimaAttribute($value)
  {
    $this->attributes['nama_penerima'] = ucwords($value);
  }

  public function setAlamatPenerimaAttribute($value)
  {
    $this->attributes['alamat_penerima'] = strtolower($value);
  }

}
