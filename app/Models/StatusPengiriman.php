<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusPengiriman extends Model
{
    protected $table       = "statuspengiriman";
    protected $primaryKey  = "id";
    public $timestamps     = false;

    public function pengiriman()
    {
      return $this->hasMany('App\Models\Pengiriman', 'statuspengiriman_id', 'id');
    }

}
