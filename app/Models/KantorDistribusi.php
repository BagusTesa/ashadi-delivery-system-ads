<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KantorDistribusi extends Model
{
  protected $table       = "kantordistribusi";
  protected $fillable    = ['nama', 'alamat', 'kota_id', 'telepon', 'lat', 'long'];
  protected $dates       = ['created_at', 'updated_at'];
  protected $casts       = ['lat'             => 'double',
                            'long'            => 'double',];
  public $primaryKey     = "id";
  public $timestamps     = true;

  public function kota()
  {
    return $this->belongsTo('App\Models\Kota', 'kota_id', 'id');
  }

  Public function user()
  {
    return $this->hasMany('App\Models\User', 'kantor_id', 'id');
  }

  public function setNamaAttribute($value)
  {
    $this->attributes['nama'] = ucwords($value);
  }

  public function setAlamatAttribute($value)
  {
    $this->attributes['alamat'] = strtolower($value);
  }
}
