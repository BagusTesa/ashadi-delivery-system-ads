<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table       = "kota";
    protected $fillable    = ['nama', 'lat', 'long'];
    protected $dates       = ['created_at', 'updated_at'];
    protected $casts       = ['lat'             => 'double',
                              'long'            => 'double',];
    public $primaryKey     = "id";
    public $timestamps     = true;

    public function kantorDistribusi()
    {
      return $this->hasMany('App\Models\KantorDistribusi', 'kota_id', 'id');
    }

    public function pengirimanBerasal()
    {
      return $this->hasMany('App\Models\Pengiriman', 'kota_pengirim', 'id');
    }

    public function pengirimanTujuan()
    {
      return $this->hasMany('App\Models\Pengiriman', 'kota_penerima', 'id');
    }

    public function setNamaAttribute($value)
    {
      $this->attributes['nama'] = strtoupper($value);
    }

}
