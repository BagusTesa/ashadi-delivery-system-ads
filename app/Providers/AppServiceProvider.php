<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Webpatser\Uuid\Uuid;
use App\Models\StatusPengiriman;
use App\Models\Pengiriman;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Pengiriman::creating(function($pengiriman){
          $pengiriman->{$pengiriman->getKeyName()} = Uuid::generate(4)->string;
          //hardlock on newly created package to be pending
          // $pengiriman->statuspengiriman_id         = StatusPengiriman::findOrFail(1)->id;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
