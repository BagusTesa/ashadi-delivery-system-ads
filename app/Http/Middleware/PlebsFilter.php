<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class PlebsFilter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(Auth::user()->kantor()->first() != null)
      {
        return $next($request);
      }
      return redirect(route('admin.index'));
    }
}
