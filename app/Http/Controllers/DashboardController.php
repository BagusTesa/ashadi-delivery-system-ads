<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

use App\Models\StatusPengiriman;
use App\Models\Pengiriman;

class DashboardController extends Controller
{
    public function getIndex()
    {
      $array = [
        'statuspengiriman' => StatusPengiriman::with('pengiriman')->get()
      ];
      return view('backends.dashboard', $array);
    }

    public function getAllDaftarPengiriman()
    {
      $array = [
        'pengiriman' => Pengiriman::with('status', 'kantor.kota', 'asal', 'tujuan')->orderBy('created_at', 'desc')->paginate(10),
        'editable'   => false,
        'deleteable' => false
      ];
      return view('backends.pengiriman-list', $array);
    }

    public function getDaftarPengiriman($statusID)
    {
      $array = [
        'pengiriman' => Pengiriman::with('status', 'kantor.kota', 'asal', 'tujuan')->where('statuspengiriman_id', $statusID)->orderBy('created_at', 'desc')->paginate(10),
        'editable'   => false,
        'deleteable' => false
      ];
      return view('backends.pengiriman-list', $array);
    }

    /**
    * for inter-distribution change transaction
    */
    public function getPaketMasuk()
    {
      $array = [
        'title'      => 'Paket Masuk',
        'postRoute'  => route('admin.postpaketmasuk')
      ];
      return view('backends.pengiriman-form-mobility', $array);
    }

    /**
    * for inter-distribution change transaction
    */
    public function postPaketMasuk(Request $request)
    {
      $this->validate($request, [
        'token'  => 'required|exists:pengiriman,token',
      ]);
      //it should be on delivery, not on office, and exists
      $oPengiriman = Pengiriman::where('token', $request->input('token'))->where('statuspengiriman_id', 2)->where('kantor_id', '<>', Auth::user()->kantor()->firstOrFail()->id)->firstOrFail();
      //not mass assigned
      $oPengiriman->statuspengiriman_id = StatusPengiriman::findOrFail(1)->id; //mark as on delivery
      $oPengiriman->kantor_id = Auth::user()->kantor()->firstOrFail()->id;
      $oPengiriman->update();

      if(Auth::user()->kantor()->firstOrFail()->kota()->firstOrFail()->id == $oPengiriman->kota_penerima)
      {
        //notify recipient, that his package existed in our distribution center
        return app('app\Http\Controller\KantorPenerima')->notifyMail($request->input('token'));
      }

      $request->session()->flash('success', trans('ads.crud.update.success', ['item' => $request->input('token')]));
      return redirect(route('admin.pengiriman.index'));
    }

    /**
    * for inter-distribution change transaction
    */
    public function getPaketKeluar()
    {
      $array = [
        'title'      => 'Paket Masuk',
        'postRoute'  => route('admin.postpaketkeluar')
      ];
      return view('backends.pengiriman-form-mobility', $array);
    }

    /**
    * for inter-distribution change transaction
    */
    public function postPaketKeluar(Request $request)
    {
      $this->validate($request, [
        'token' => 'required|exists:pengiriman,token'
      ]);
      //it should be pending, on the respective office, and exists
      $oPengiriman = Pengiriman::where('token', $request->input('token'))->where('statuspengiriman_id', 1)->where('kantor_id', Auth::user()->kantor()->firstOrFail()->id)->firstOrFail();
      //not mass assigned
      $oPengiriman->statuspengiriman_id = StatusPengiriman::findOrFail(2)->id; //mark as on delivery
      $oPengiriman->kantor_id = Auth::user()->kantor()->firstOrFail()->id;
      $oPengiriman->update();

      if(Auth::user()->kantor()->firstOrFail()->kota()->firstOrFail()->id == $oPengiriman->kota_penerima)
      {
        //notify recipient, that his package somehow left our distribution center
        return app('app\Http\Controller\KantorPenerima')->notifyMail($request->input('token'));
      }

      $request->session()->flash('success', trans('ads.crud.update.success', ['item' => $request->input('token')]));
      return redirect(route('admin.pengiriman.index'));
    }

}
