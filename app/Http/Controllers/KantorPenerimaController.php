<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\StatusPengiriman;
use App\Models\Pengiriman;
use App\Models\Kota;
use App\Models\KantorDistribusi;

/**
* Handle site backend, related to end-point office, update it's status, show routes
*/
class KantorPenerimaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $array = [
        'pengiriman'  => Pengiriman::with('status', 'kantor', 'asal', 'tujuan')->where('token', $id)->firstOrFail(),
        'directions'  => true
      ];
      return view('backends.pengiriman-detail', $array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
      $oPengiriman = Pengiriman::with('status')->where('token', $id)->firstOrFail();
      $array = [
        'pengiriman'       => Pengiriman::with('status')->where('token', $id)->firstOrFail(),
        //only capable to change it either sent or failed
        'statuspengiriman' => StatusPengiriman::where('id', 1)->orWhere('id', 3)->orWhere('id', 4)->get()
      ];
      $request->merge($oPengiriman->getAttributes());
      return view('backends.pengiriman-form-status-only', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $oPengiriman = Pengiriman::findOrFail($id);
      $this->validate($request, [
        'statuspengiriman_id'   => 'required|exists:statuspengiriman,id',
      ]);
      //not mass assigned style
      $oPengiriman->statuspengiriman_id = $request->input('statuspengiriman_id');
      $oPengiriman->update();

      $this->notifyMail($id);

      $request->session()->flash('success', trans('ads.crud.update.success', ['item' => $id]));
      return redirect(route('admin.pengiriman.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function notifyMail($id)
    {
      $oPengirim = Pengirim::with('kantor', 'asal', 'tujuan', 'status')->where('token', $id)->firstOrFail();

      if($oPengiriman->email_penerima && config('ads.ENABLE_MAIL'))
      {
        Mail::queue('emails.pengiriman-status', ['pengiriman' => $oPengiriman], function($mail) use($oPengiriman){
          $mail->from(config('ads.MAIL_SENDER'), 'Ashadi Delivery System');
          $mail->to($oPengiriman->email_penerima, $oPengiriman->nama_penerima);
          $mail->subject('Informasi Pengiriman');
        });
      }

      return true;
    }
}
