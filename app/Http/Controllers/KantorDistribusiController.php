<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\KantorDistribusi;
use App\Models\Kota;
use App\Models\User;

class KantorDistribusiController extends Controller
{
    protected $validation_create = [
      'nama'    => 'required|max:255',
      'alamat'  => 'required|max:255',
      'kota_id' => 'required|exists:kota,id',
      'telepon' => 'required|max:255',
      'lat'     => 'required|numeric',
      'long'    => 'required|numeric',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array = [
          'kantor' => KantorDistribusi::with('user', 'kota')->paginate(10)
        ];
        return view('backends.kantor-list', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $array = [
          'kota' => Kota::all()
        ];
        return view('backends.kantor-form', $array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validation_create);
        KantorDistribusi::create($request->all());
        $request->session()->flash('success', trans('ads.crud.creation.success', ['item' => 'Kantor Distribusi']));
        return redirect(route('admin.kantordistribusi.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $array = [
          'kantor'  => KantorDistribusi::with('user', 'kota.kantorDistribusi')->where('id', $id)->firstOrFail()
        ];
        return view('backends.kantor-detail', $array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $kantor = KantorDistribusi::findOrFail($id);
        $array  = [
          'kantor'           => $kantor,
          'kota'             => Kota::all(),
          'kantorDistribusi' => kantorDistribusi::with('kota')->firstOrFail()
        ];
        $request->merge($kantor->getAttributes());
        return view('backends.kantor-form', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, $this->validation_create);
      KantorDistribusi::findOrFail($id)->update($request->all());
      $request->session()->flash('success', trans('ads.crud.update.success', ['item' => 'Kantor Distribusi']));
      return redirect(route('admin.kantordistribusi.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $oKantorDistribusi = KantorDistribusi::findOrFail($id);
        $oPengirimanKantor = $oKantorDistribusi->user()->get();
        $oUserKantor       = $oKantorDistribusi->user()->get();
        if((count($oPengirimanKantor) == 0) && (count($oUserKantor) == 0))
        {
          $oKantorDistribusi->delete();
          $request->session()->flash('success', trans('ads.crud.delete.success', ['item' => 'Kantor Distribusi']));
          return redirect(route('admin.kantordistribusi.index'));
        }
        else
        {
          $request->session()->flash('danger', trans('ads.crud.delete.failed', ['item' => 'Kantor Distribusi']));
          return redirect(route('admin.kantordistribusi.index'));
        }
    }
}
