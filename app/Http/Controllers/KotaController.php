<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Kota;
use App\Models\KantorDistribusi;

class KotaController extends Controller
{
    protected $validation_create = [
      'nama'  => 'required|max:255',
      'lat'   => 'required|numeric',
      'long'  => 'required|numeric',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array = [
          'kota' => Kota::with('kantorDistribusi')->paginate(10)
        ];
        return view('backends.kota-list', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $array = [
          'kota' => Kota::all()
        ];
        return view('backends.kota-form', $array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validation_create);
        Kota::create($request->all());
        $request->session()->flash('success', trans('ads.crud.creation.success', ['item' => 'Kota']));
        return redirect(route('admin.kota.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $array = [
          'kota' => Kota::with('kantorDistribusi', 'pengirimanBerasal', 'pengirimanTujuan')->where('id', $id)->firstOrFail()
        ];
        return view('backends.kota-detail', $array);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
      $array = [
        'kota' => Kota::findOrFail($id)
      ];
      $request->merge(Kota::findOrFail($id)->getAttributes());
      return view('backends.kota-form', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, $this->validation_create);
      $oKota = Kota::findOrFail($id);
      $oKota->update($request->all());
      $request->session()->flash('success', trans('ads.crud.update.success', ['item' => 'Kota']));
      return redirect(route('admin.kota.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $oKota = Kota::findOrFail($id);
        if((count($oKota->kantorDistribusi()->get()) == 0) && (count($oKota->pengirimanTujuan()->get()) == 0) && (count($oKota->pengirimanBerasal()->get()) == 0))
        {
          $oKota->delete();
          $request->session()->flash('success', trans('ads.crud.delete.success', ['item' => 'Kota']));
          return redirect(route('admin.kota.index'));
        }
        $request->session()->flash('danger', trans('ads.crud.delete.failed', ['item' => 'Kota']));
        return redirect(route('admin.kota.index'));
    }
}
