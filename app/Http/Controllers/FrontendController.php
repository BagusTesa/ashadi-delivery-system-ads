<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\Pengiriman;
use App\Models\StatusPengiriman;
use App\Models\KantorDistribusi;

/**
* Handle site frontends, related to branding etc etc
*/
class FrontendController extends Controller
{
    /**
     * Display homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
      $array = [
        'statuspengiriman' => StatusPengiriman::with('pengiriman')->get()
      ];
      return view('frontends.home', $array);
    }

    /**
    *  Display aboutUs page
    */
    public function getAboutUs()
    {
      return view('frontends.aboutus');
    }

    /**
    *  Display list of distribution office
    */
    public function getKantor()
    {
      $array = [
        'kantor' => KantorDistribusi::with('kota')->orderBy('nama', 'asc')->paginate(10)
      ];
      return view('frontends.kantor', $array);
    }

    /**
    * Display tracking page
    */
    public function getTracking()
    {
      return view('frontends.tracking');
    }

    /**
    * Display tracked object, for frontend use
    */
    public function postTracking(Request $request)
    {
      $this->validate($request, [
        'token'  => 'required|max:36|min:36|exists:pengiriman|regex:/([a-f\d]{8}(-[a-f\d]{4}){3}-[a-f\d]{12}?)/' //token is a uuid: 8-4-4-4-16, not a valid regex for uuid, but should do
      ]);
      //real regex for uuid: /([a-f\d]{8}(-[a-f\d]{4}){3}-[a-f\d]{12}?)/ig (unfortunately, laravel uses preg_match instead of preg_match_all so, no ivil genius (ig) support)
      $array = [
        'pengiriman' => Pengiriman::with('status', 'kantor.kota', 'asal', 'tujuan')->where('token', $request->input('token'))->firstOrFail()
      ];
      return view('frontends.tracking', $array);
    }

    public function getKonfirmasi($token, Request $request)
    {
      //show page to confirm package via e-mail url
      $pengiriman = Pengiriman::with('status', 'kantor.kota', 'asal', 'tujuan')->where('token', $token)->firstOrFail();
      $array = [
        'pengiriman' => $pengiriman
      ];
      if($pengiriman->emailkonfirmasi)
      {
        //it already confirmed, nothing to do - but cuddling a loli is a must
        $request->session()->flash('danger', trans('ads.crud.update.failed', ['item' => 'Data Pengiriman']));
        return redirect(route('home'));
      }
      $request->merge($pengiriman->getAttributes());
      return view('frontends.konfirmasi', $array);
    }

    public function postKonfirmasi($token, Request $request)
    {
      //validate and such
      $this->validate($request, [
        'lat'        => 'required|numeric',
        'long'       => 'required|numeric',
        'konfirmasi' => 'required',
      ]);
      $pengiriman = Pengiriman::where('token', $token)->firstOrFail();
      $pengiriman->update($request->except('konfirmasi'));
      $pengiriman->emailkonfirmasi = true;
      return view('frontends.home');
    }
}
