<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\StatusPengiriman;
use App\Models\Pengiriman;
use App\Models\Kota;
use App\Models\KantorDistribusi;

/**
* Handle site backend, related to intermedietary office, mark where the package now and it's status
*/
class KantorPerantaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
      $oPengiriman = Pengiriman::with('status')->where('token', $id)->firstOrFail();
      $array = [
        'pengiriman'       => Pengiriman::with('status')->where('token', $id)->firstOrFail(),
        //only capable to change it either pending or on delivery
        'statuspengiriman' => StatusPengiriman::where('id', 1)->orWhere('id', 2)->get()
      ];
      $request->merge($oPengiriman->getAttributes());
      return view('backends.pengiriman-form-status-only', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oPengiriman = Pengiriman::findOrFail($id);
        $this->validate($request, [
          'statuspengiriman_id'   => 'required|exists:statuspengiriman,id',
        ]);
        //not mass assigned style
        $oPengiriman->statuspengiriman_id = $request->input('statuspengiriman_id');
        $oPengiriman->update();
        $request->session()->flash('success', trans('ads.crud.update.success', ['item' => $id]));
        return redirect(route('admin.pengiriman.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
