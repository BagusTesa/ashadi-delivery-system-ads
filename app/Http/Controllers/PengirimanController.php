<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Mail;
use App\Models\StatusPengiriman;
use App\Models\Pengiriman;
use App\Models\Kota;
use App\Models\KantorDistribusi;
/**
*  Multiplexes edit and update requests according to user who logged in (dirty approach - well, i'm lolicon after all)
*
*/
class PengirimanController extends Controller
{
    protected $validation_create = [
      'nama_pengirim'    => 'required|max:255',
      'alamat_pengirim'  => 'required|max:255',
      'telepon_pengirim' => 'max:255',
      'nama_penerima'    => 'required|max:255',
      'alamat_penerima'  => 'required|max:255',
      'kota_penerima'    => 'required|exists:kota,id',
      'telepon_penerima' => 'max:255',
      'email_penerima'   => 'email|max:255',
      'lat_penerima'     => 'required_with:long_penerima|numeric',
      'long_penerima'    => 'required_with:lat_penerima|numeric',
      'volume_barang'    => 'required|digits_between:1,5',
      'berat_barang'     => 'required|digits_between:1,5',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array = [
          'pengiriman'  => Pengiriman::with('status', 'asal', 'tujuan', 'kantor.kota')->where('kantor_id', Auth::user()->kantor()->firstOrFail()->id)->where('statuspengiriman_id', 1)->orderBy('created_at', 'desc')->paginate(10),
          'editable'    => true
        ];
        return view('backends.pengiriman-list', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $array = [
          'kota'   => Kota::all()
        ];
        return view('backends.pengiriman-form', $array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validation_create);
        $nPengiriman                      = new Pengiriman();
        //fill, fill, fill, fill, fill, fillable 5 times
        $nPengiriman->fill($request->all());
        //but each it filled
        //always assume it being sent from this office!
        $nPengiriman->kantor_id           = Auth::user()->kantor_id;
        $nPengiriman->kota_pengirim       = Auth::user()->kantor()->firstOrFail()->kota()->firstOrFail()->id;
        //should be moved to hardlock if possible
        $nPengiriman->statuspengiriman_id =  StatusPengiriman::findOrFail(1)->id;
        $nPengiriman->save();

        $nPengiriman = Pengiriman::with('kantor', 'asal', 'tujuan', 'status')->where('token', $id)->firstOrFail();

        if(($nPengiriman->email_penerima) && config('ads.ENABLE_MAIL'))
        {
          Mail::queue('emails.pengiriman', ['pengiriman' => $nPengiriman], function($mail) use($nPengiriman){
            $mail->from(config('ads.MAIL_SENDER'), 'Ashadi Delivery System');
            $mail->to($nPengiriman->email_penerima, $nPengiriman->nama_penerima);
            $mail->subject('Konfirmasi Informasi Pengiriman');
          });
        }

        return redirect(route('admin.pengiriman.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $usrKota = Auth::user()->kantor()->firstOrFail()->kota()->firstOrFail()->id;
      $pengiriman = Pengiriman::findOrFail($id);
      if(($usrKota == $pengiriman->kota_penerima) && ($usrKota == $pengiriman->kantor()->firstOrFail()->kota()->firstOrFail()->id))
      {
        //throw a view that capable to render directional map
        return app('App\Http\Controllers\KantorPenerimaController')->show($id);
      }
      else
      {
        //something else
        $array = [
          'pengiriman'  => Pengiriman::with('status', 'kantor', 'asal', 'tujuan')->where('token', $id)->firstOrFail()
        ];
        return view('backends.pengiriman-detail', $array);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $usrKota = Auth::user()->kantor()->firstOrFail()->kota()->firstOrFail()->id;
        $pengiriman = Pengiriman::findOrFail($id);
        if(($usrKota == $pengiriman->kota_pengirim) && ($usrKota == $pengiriman->kantor()->firstOrFail()->kota()->firstOrFail()->id))
        {
          //kantor pengirim, barang belum berpindah tempat
          return app('App\Http\Controllers\KantorPengirimController')->edit($request, $id);
        }
        elseif(($usrKota == $pengiriman->kota_penerima)  && ($usrKota == $pengiriman->kantor()->firstOrFail()->kota()->firstOrFail()->id))
        {
          //kantor penerima, barang telah sampai
          return app('App\Http\Controllers\KantorPenerimaController')->edit($request, $id);
        }
        else
        {
          //kantor perantara
          return app('App\Http\Controllers\KantorPerantaraController')->edit($request, $id);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $usrKota = Auth::user()->kantor()->firstOrFail()->kota()->firstOrFail()->id;
      $pengiriman = Pengiriman::findOrFail($id);
      if(($usrKota == $pengiriman->kota_pengirim) && ($usrKota == $pengiriman->kantor()->firstOrFail()->kota()->firstOrFail()->id))
      {
        //sender office
        return app('App\Http\Controllers\KantorPengirimController')->update($request, $id);
      }
      elseif(($usrKota == $pengiriman->kota_penerima) && ($usrKota == $pengiriman->kantor()->firstOrFail()->kota()->firstOrFail()->id))
      {
        //recipient office
        return app('App\Http\Controllers\KantorPenerimaController')->update($request, $id);
      }
      else
      {
        //intermedietary office
        return app('App\Http\Controllers\KantorPerantaraController')->update($request, $id);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usrKota = Auth::user()->kantor()->firstOrFail()->kota()->firstOrFail()->id;
        $pengirman = Pengiriman::findOrFail($id);
        if(($usrKota == $pengiriman->kota_pengirim) && ($usrKota == $pengiriman->kantor()->firstOrFail()->kota()->firstOrFail()->id))
        {
          //only be removed in initial office, no removal allowed in transit or in recipient office
          return app('App\Http\Controllers\KantorPengirim')->destroy($id);
        }
    }
}
