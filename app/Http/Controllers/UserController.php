<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Hash;

use App\Models\User as User;
use App\Models\KantorDistribusi as KantorDistribusi;

class UserController extends Controller
{

    public function getLogin()
    {
      if(Auth::user() != null)
      {
        return redirect(route('admin.index'));
      }
      else
      {
        return view('backends.login');
      }
    }

    public function postLogin(Request $request)
    {
      $this->validate($request, [
        'email'   => 'required|email|max:255|exists:users,email',
        'password'=> 'required|max:255',
      ]);

      if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
      {
        return redirect(route('admin.index'));
      }
      else
      {
        $request->session()->flash('danger', trans('ads.login.failed'));
        return redirect(route('login'));
      }

    }

    public function getLogout(Request $request)
    {
      $request->session()->flush();
      Auth::logout();
      return redirect()->route('login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array = [
          'user' => User::with('kantor.kota')->paginate(10)
        ];
        return view('backends.user-list', $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $array = [
          'kantor'  => KantorDistribusi::with('kota')->get()
        ];
        return view('backends.user-form', $array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name'      => 'required|max:255',
          'email'     => 'required|email|max:255|unique:users',
          'password'  => 'required|confirmed|min:6',
          'kantor_id' => 'required|exists:kantordistribusi,id',
        ]);
        User::create($request->all());
        $request->session()->flash('success', trans('ads.crud.creation.success', ['item' => 'user']));
        return redirect(route('admin.user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::with('kantor')->where('id', $id)->firstOrFail();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user  = User::with('kantor')->where('id', $id)->firstOrFail();
        $array = [
          'user'   => $user,
          'kantor' => KantorDistribusi::all()
        ];
        $request->merge($user->getAttributes());
        return view('backends.user-form', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name'         => 'required|max:255',
          'email'        => 'required|email|max:255',
          'old_password' => 'required_with:password|min:6',
          'password'     => 'confirmed|required_with:old_password|min:6',
          'kantor_id'    => 'exists:kantordistribusi,id',
        ]);

        $oUser = User::findOrFail($id);
        if(strlen($request->input('password')) > 0 )
        {
          //check only if required
          if(Hash::check($request->input('old_password'), $oUser->password))
          {
            $oUser->update($request->except(['old_password', 'password_confirmation']));
          }
          else
          {
            goto hash_check_fail;
          }
        }
        $oUser->update($request->except(['old_password', 'password', 'password_confirmation']));
        $request->session()->flash('success', trans('ads.crud.update.success', ['item' => 'user']));
        return redirect(route('admin.user.index'));
        hash_check_fail:
        return redirect(route('admin.user.edit', $id))->withErrors(['password' => trans('validation.custom.password.old')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $oUser = User::findOrFail($id);
        $oUser->delete();
        $request->session()->flash('warning', trans('ads.crud.delete.success', ['item' => 'user']));
        return redirect(route('admin.user.index'));
    }
}
