<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Mail;
use App\Models\StatusPengiriman;
use App\Models\Pengiriman;
use App\Models\Kota;
use App\Models\KantorDistribusi;

/**
* Handle site backends, utilised for new registered package to be sent
*/
class KantorPengirimController extends PengirimanController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $pengirim = Pengiriman::with('kantor.kota', 'asal', 'tujuan', 'status')->where('token', $id)->firstOrFail();
        $array = [
          'pengiriman'       => $pengirim,
          'kota'             => Kota::all(),
        ];
        $request->merge($pengirim->getAttributes());
        return view('backends.pengiriman-form', $array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oPengiriman = Pengiriman::findOrFail($id);
        $this->validate($request, $this->validation_create);
        $oPengiriman->update($request->all());

        $oPengiriman = Pengiriman::with('kantor', 'asal', 'tujuan', 'status')->where('token', $id)->firstOrFail();

        if($oPengiriman->email_penerima && config('ads.ENABLE_MAIL'))
        {
          Mail::queue('emails.pengiriman', ['pengiriman' => $oPengiriman], function($mail) use($oPengiriman){
            $mail->from(config('ads.MAIL_SENDER'), 'Ashadi Delivery System');
            $mail->to($oPengiriman->email_penerima, $oPengiriman->nama_penerima);
            $mail->subject('Update Informasi Pengiriman');
          });
        }

        $request->session()->flash('success', trans('ads.crud.update.success', ['item' => $id]));
        return redirect(route('admin.pengiriman.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oPengirim = Pengirim::findOrFail($id);
        $oPengirim->destroy();
    }
}
