<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('login', [
  'as'   => 'login',
  'uses' => 'UserController@getLogin'
]);

Route::post('login', [
  'as'   => 'login',
  'uses' => 'UserController@postLogin'
]);

Route::get('logout', [
  'as'   => 'logout',
  'uses' => 'UserController@getLogout'
]);

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function(){
    Route::group(['middleware' => 'plebs'], function(){
      Route::resource('pengiriman', 'PengirimanController');
    });

    Route::group(['middleware' => 'superuser'], function(){
      Route::resource('kantordistribusi', 'KantorDistribusiController');
      Route::resource('kota', 'KotaController');
      Route::resource('user', 'UserController', [
        'except' => ['show']
      ]);
    });

    //getIndex and getAllDaftarPengiriman can be done in more laravelistic using nested resource controller
    Route::controller('/', 'DashboardController', [
      'getIndex'               => 'admin.index',
      'getAllDaftarPengiriman' => 'admin.all-pengiriman',
      'getDaftarPengiriman'    => 'admin.status-pengiriman',
      'getPaketMasuk'          => 'admin.getpaketmasuk',
      'postPaketMasuk'         => 'admin.postpaketmasuk',
      'getPaketKeluar'         => 'admin.getpaketkeluar',
      'postPaketKeluar'        => 'admin.postpaketkeluar',
    ]);
});

Route::controller('/', 'FrontendController', [
  'getIndex'      => 'home',
  'getAboutUs'    => 'aboutus',
  'getKantor'     => 'kantor',
  'getTracking'   => 'tracking',
  'postTracking'  => 'tracking',
  'getKonfirmasi' => 'konfirmasi',
  'postKonfirmasi'=> 'konfirmasi',
]);
